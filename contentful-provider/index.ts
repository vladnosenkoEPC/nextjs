import { client } from "./client";

const getPageContent = () => {
  return client.getEntries({
    content_type: "landingPage",
  });
};

const getCurrentPageContent = (page: string) => {
  return client.getEntries({
    content_type: "landingPage",
    "fields.path[match]": page,
  });
};

export { getPageContent, getCurrentPageContent };
