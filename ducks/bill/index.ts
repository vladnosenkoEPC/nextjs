import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type InitialState = {
  regularPlanId: string;
  upSalePlanId: string;
  downSalePlanId: string;
};

const initialState: InitialState = {
  regularPlanId: "",
  upSalePlanId: "",
  downSalePlanId: "",
};

const bill = createSlice({
  name: "bill",
  initialState,
  reducers: {
    resetBillStore: () => initialState,
    setRegularPlanId: (state, action: PayloadAction<string>) => {
      state.regularPlanId = action.payload;
    },
    setUpSalePlanId: (state, action: PayloadAction<string>) => {
      state.upSalePlanId = action.payload;
    },
    setDownSalePlanId: (state, action: PayloadAction<string>) => {
      state.downSalePlanId = action.payload;
    },
  },
});

export const {
  resetBillStore,
  setRegularPlanId,
  setUpSalePlanId,
  setDownSalePlanId,
} = bill.actions;

export default bill.reducer;
