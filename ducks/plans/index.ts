import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type InitialState = {
  title: string;
  planId: string;
  price: string;
  priceAmount: number;
  category: string;
  name: string;
  sku: string;
};

const plan = createSlice({
  name: "plan",
  initialState: <InitialState[]>[],
  reducers: {
    resetPlanStore: () => [],
    setPlanInfo: (state, action: PayloadAction<InitialState[]>) => {
      return [...state, ...action.payload];
    },
  },
});

export const { resetPlanStore, setPlanInfo } = plan.actions;

export default plan.reducer;
