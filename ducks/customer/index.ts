import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type InitialState = {
  paymentToken: string;
  userEmail: string;
  userId: string;
};

const initialState: InitialState = {
  paymentToken: "",
  userEmail: "",
  userId: "",
};

const customer = createSlice({
  name: "customer",
  initialState,
  reducers: {
    resetCustomerStore: (state) => {
      state.paymentToken = initialState.paymentToken;
    },
    setPaymentToken: (state, action: PayloadAction<string>) => {
      state.paymentToken = action.payload;
    },
    setUserEmail: (state, action: PayloadAction<string>) => {
      state.userEmail = action.payload;
    },
    setUserId: (state, action: PayloadAction<string>) => {
      state.userId = action.payload;
    },
  },
});

export const { resetCustomerStore, setPaymentToken, setUserEmail, setUserId } = customer.actions;

export default customer.reducer;
