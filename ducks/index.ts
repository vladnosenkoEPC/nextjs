import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { reducer as toastr } from "react-redux-toastr";

import bill from "./bill";
import customer from "./customer";
import plan from "./plans";

const reducer = combineReducers({
  toastr,
  bill,
  customer,
  plan,
});

const store = configureStore({
  reducer,
  middleware: [],
});

export * from "./customer";
export * from "./plans";
export * from "./bill";
export type RootState = ReturnType<typeof reducer>;
export default store;
