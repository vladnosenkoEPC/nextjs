const appEnv = process.env.REACT_APP_ENV || "development";

interface Environment {
  apiUrl: string;
  chargebeeSite: string;
  chargebeeApiKey: string;
}

interface Environments {
  [key: string]: Environment;
}

const environments: Environments = {
  development: {
    apiUrl: process.env.REACT_APP_API || "http://localhost:5000/api",
    chargebeeSite: process.env.REACT_APP_CHARGEBEE_SITE || "phoenixfr",
    chargebeeApiKey:
      process.env.REACT_APP_CHARGEBEE_API_KEY || "live_1g4BG2O7hFVy91XFFcdsPrcuGdPliAWPlcd",
  },
  test: {
    apiUrl: "http://13.58.25.75:5000/api",
    chargebeeSite: process.env.REACT_APP_CHARGEBEE_SITE || "phoenixfr-test",
    chargebeeApiKey:
      process.env.PREACT_APP_CHARGEBEE_API_KEY || "test_fMQa6UklFaz28a61FFmnSX0D0Q7Ph7rS",
  },
  staging: {
    apiUrl: "https://api.phoenixfinancialresearch.dev/api",
    chargebeeSite: process.env.REACT_APP_CHARGEBEE_SITE || "phoenixfr-test",
    chargebeeApiKey:
      process.env.REACT_APP_CHARGEBEE_API_KEY || "test_fMQa6UklFaz28a61FFmnSX0D0Q7Ph7rS",
  },
  prod: {
    apiUrl: "https://api.phoenixfinancialresearch.com/api",
    chargebeeSite: process.env.REACT_APP_CHARGEBEE_SITE || "phoenixfr",
    chargebeeApiKey:
      process.env.REACT_APP_CHARGEBEE_API_KEY || "live_1g4BG2O7hFVy91XFFcdsPrcuGdPliAWPlcd",
  },
};

export const appEnvironment: Environment = environments[appEnv];
