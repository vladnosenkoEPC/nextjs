import { lazy } from "react";
import NotFound from "../pages/not-found";

// const SPT1 = lazy(() => import("../sales-pages/spt-1"));

import SPT1 from "../sales-pages/spt-1";

export interface RouteConfig {
  path: string;
  component: any;
  name: string;
  default?: boolean;
  exact: boolean;
}

export const routes: RouteConfig[] = [
  // {
  //   path: "/spt-1",
  //   component: SPT1,
  //   name: "Sales Page 1",
  //   exact: false,
  // },
  {
    path: "/spt-1",
    component: SPT1,
    name: "Not Found",
    exact: true,
  },
];