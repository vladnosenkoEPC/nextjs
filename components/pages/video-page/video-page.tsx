import React, { useState, useRef, lazy } from "react";
import { useHistory } from "react-router-dom";
import { useDebounce } from "components";
import Image from 'next/image'

import { scrollToElement } from "utils/scroll.utils";

import styles from "./video-page.module.css";

import playBtnImg from "/assets/images/video-page-play-btn.svg";
import playerArrowImg from "/assets/images/video-page-player-arrow.svg";
import playerTextImg from "/assets/images/video-page-player-text.svg";
import exitPopupCheckmarkImg from "/assets/images/checkmark.svg";

const VideoWidget = lazy(() => import("components/ui/video-widget/video-widget"));
const Button = lazy(() => import("components/ui/button/button"));
const ExitPopupModal = lazy(() => import("components/ui/exit-popup-modal/exit-popup-modal"));


interface VideoWidgetSettings {
  btnShowTime: number;
  link: string;
  id: string;
}

const videoWidgetSettings: VideoWidgetSettings = {
  btnShowTime: 2602,
  link: "https://player.vimeo.com/video/524191163",
  id: "player-524191163",
};

const longReadPath = `/spt-1/transcript/how-a-brain-tumor-helped-me-crack-wall-street`;
const checkoutPath = `/spt-1/order-page/strategic-profits-trader`;

const scrollToInfoTextDelay = 100;

const VideoPage = () => {
  const [exitPopupStatus, setExitPopupStatus] = useState(false);
  const [downInfoTextStatus, setDownInfoTextStatus] = useState(false);
  const [pageBgStatus, setPageBgStatus] = useState(false);
  const currentYear = new Date().getFullYear();
  const downInfoTextRef = useRef<any>(null);
  const { debounce } = useDebounce(scrollToInfoTextDelay);

  const history = useHistory();

  const changePageBgStatus = (newStatus: boolean) => {
    setPageBgStatus(newStatus);
  };

  const handleClose = () => {
    setExitPopupStatus(false);
  };

  const showDownInfoText = () => {
    if (!downInfoTextStatus) {
      setDownInfoTextStatus(true)
      debounce(() => {
        scrollToElement(downInfoTextRef.current);
      });
    } else {
      setDownInfoTextStatus(false)
    }
  };

  const handleReadTranscript = () => {
    history.push(longReadPath);
  };

  return (
    <div className={`${styles.pageWrapper} ${pageBgStatus && styles.pageWrapperDark}`}>
      <Image className={styles.pageBg} src={"/assets/images/video-page-bg.jpg"} alt="" />
      <div className={styles.container}>
        <h1 className={styles.header}>How A Brain Tumor Helped Me Crack Wall Street</h1>
        <p className={styles.subheader}>
          And how my misfortune can be <span className={styles.underline}>your edge in 2021</span>
        </p>
        <VideoWidget
          btnShowTime={videoWidgetSettings.btnShowTime}
          link={videoWidgetSettings.link}
          id={videoWidgetSettings.id}
          changePageBgStatus={changePageBgStatus}
          longReadPath={longReadPath}
          checkoutPath={checkoutPath}
          redirect={true}
        >
          <img className={styles.playBtn} src={playBtnImg} alt="" />
          <img className={styles.playerArrow} src={playerArrowImg} alt="" />
          <img className={styles.playerText} src={playerTextImg} alt="" />
        </VideoWidget>
        <div className={styles.downInfo}>
          <p
            className={`${styles.downInfoHeader} ${pageBgStatus && styles.downInfoHeaderVisibility
              }`}
            onClick={showDownInfoText}
          >
            Company Information
          </p>
          <p
            className={`${styles.downInfoText} ${downInfoTextStatus && styles.activeDownInfoText}`}
            ref={downInfoTextRef}
          >
            © {currentYear} Strategic Profits Trader. All rights reserved. Protected by copyright
            laws of the United States and international treaties. This website may only be used
            pursuant to the subscription agreement and any reproduction, copying, or redistribution
            (electronic or otherwise, including on the world wide web), in whole or in part, is
            strictly prohibited without the express written permission of Strategic Profits Trader.
          </p>
        </div>
        <ExitPopupModal isOpen={exitPopupStatus} setClose={setExitPopupStatus}>
          <div className={`${styles.exitPopup}`}>
            <p className={styles.popupTitle}>
              <span className={styles.popupTitleRed}>WAIT – DON’T MISS OUT!</span>
            </p>
            <p className={styles.popupText}>
              <span className={styles.popupTextBold}>
                Don’t leave before you discover how Bob’s 10-Stock System delivers annualized
                142%... 203%... 300% or more – by doing all the heavy lifting for you.
              </span>
            </p>
            <p className={styles.popupText}>You’re about to discover the system that: </p>
            <p className={styles.popupText}>
              <img src={exitPopupCheckmarkImg} alt="" />
              <span className={styles.popupTextBold}>
                Eliminates <span className={styles.italic}>fear, stress</span>
              </span>{" "}
              and <span className={styles.popupTextBold}>uncertainty</span> from trading…
            </p>
            <p className={styles.popupText}>
              <img src={exitPopupCheckmarkImg} alt="" />
              Keep losses to a
              <span className={`${styles.popupTextBold} ${styles.italic}`}> minimum…</span>
            </p>
            <p className={styles.popupText}>
              <img src={exitPopupCheckmarkImg} alt="" />
              Profits to a{" "}
              <span className={`${styles.popupTextBold} ${styles.italic}`}>maximum…</span>
            </p>
            <p className={styles.popupText}>
              <img src={exitPopupCheckmarkImg} alt="" />
              And can help anyone{" "}
              <span className={`${styles.popupTextBold} ${styles.italic}`}>survive</span> and{" "}
              <span className={`${styles.popupTextBold} ${styles.italic}`}>thrive</span> the ups and
              downs of the news cycles...
            </p>
            <p className={styles.popupText}>
              <span className={styles.popupTextBold}>
                Plus, you’ll also have a chance to get the next batch of trade picks that could
                deliver 2X, 5X even 10X gains within weeks.
              </span>
            </p>
            <p className={styles.popupText}>
              Click the “Continue Watching” button to keep watching.
            </p>
            <p className={styles.popupText}>
              Or click “Read Transcript” button to read the details yourself.
            </p>
            <div className={styles.popupBtnContainer}>
              <Button
                className={styles.popupBtn}
                btnSize="big"
                loader={false}
                onClick={handleReadTranscript}
              >
                Read Transcript
              </Button>
              <Button
                className={`${styles.popupBtn} ${styles.popupBtnSecond}`}
                btnSize="big"
                loader={false}
                onClick={handleClose}
              >
                Continue Video
              </Button>
            </div>
          </div>
        </ExitPopupModal>
      </div>
    </div>
  );
};

export default VideoPage;
