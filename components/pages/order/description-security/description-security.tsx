import React from "react";
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';

import { companySupportEmail } from "/constants/company-info.constant";

import styles from "./description-security.module.css";

type Props = {
  content: any;
};

const DescriptionSecurity = ({ content }: Props) => {
  const emailLink = "mailto:" + companySupportEmail;
  const text = documentToReactComponents(content && content[6].fields.content);

  return (
    <div className={styles.container}>
      <img src="/assets/images/security-lock.svg" />
      <h3 className={styles.lockSubtitle}>SECURE PAYMENT</h3>
      <img className={styles.securityImg} src="/assets/images/order-secure-payment.svg" alt="" />
      <div className={styles.text}>
        {text}
        <a className={styles.link} href={emailLink}>
          click here.
        </a>
      </div>
    </div>
  );
};

export default DescriptionSecurity;
