import React from "react";

import { companySupportEmail } from "constants/company-info.constant";

import styles from "./description-security.module.css";

import SecurityIcon1 from "assets/images/security-icon-1.svg";
import SecurityIcon2 from "assets/images/security-icon-2.svg";
import SecurityIcon3 from "assets/images/security-icon-3.svg";

const DescriptionSecurityGrid = () => {
  const emailLink = "mailto:" + companySupportEmail;

  return (
    <div className={styles.securityGrid}>
      <div className={styles.gridItem}>
        <div>
          <img src={SecurityIcon2} alt="" />
        </div>
        <div>
          <div className={styles.title}>Your Information Is Safe</div>
          <p className={styles.securityText}>
            We will not add your name to our e-mail list without your permission. We will not sell,
            rent, or otherwise share your e-mail address with anyone.
          </p>
        </div>
      </div>
      <div className={styles.gridItem}>
        <div>
          <img src={SecurityIcon3} alt="" />
        </div>
        <div>
          <div className={styles.title}>Need Help?</div>
          <p className={styles.securityText}>
            Call Us at (888) 261-2693
            <div> 9am -- 5pm EST</div>
            <div>Monday -- Friday</div>
            <a href={emailLink}>{companySupportEmail}</a>
          </p>
        </div>
      </div>
      <div className={styles.gridItem}>
        <div>
          <img src={SecurityIcon1} alt="" />
        </div>
        <div>
          <div className={styles.title}>Secure Checkout</div>
          <p className={styles.securityText}>
            100% Protected. SSL Protocol. All information is encrypted and transmitted without risk
            using a Secure Sockets Layer protocol.
          </p>
        </div>
      </div>
    </div>
  );
};

export default DescriptionSecurityGrid;
