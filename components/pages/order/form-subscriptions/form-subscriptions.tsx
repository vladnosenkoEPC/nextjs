import React, { useField } from "formik";

import { useSelector } from "react-redux";
import { RootState } from "ducks/index";

import { MessageField } from "components";
import FormSubscriptionsCard from "../form-subscriptions-card/form-subscriptions-card";

import styles from "./form-subscriptions.module.css";

const name = "planId";

const FormSubscriptions = () => {
  const [, meta] = useField({ name });
  const { touched, error } = meta;
  const errorMessage = (touched && error) || "";

  const plansList = useSelector((state: RootState) => state.plan);

  return (
    <div>
      <div className={styles.container}>
        {plansList && plansList.map(card => (
          <FormSubscriptionsCard key={card.title} {...card} name={name} />
        ))}
      </div>
      <MessageField message={errorMessage} align="center" size="large" />
    </div>
  );
};

export default FormSubscriptions;
