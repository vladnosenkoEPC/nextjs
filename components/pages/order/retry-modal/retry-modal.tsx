import React, { useState } from "react";
import { useDidUpdate } from "react-hooks-lib";

import { useDebounce, Modal, Button } from "components";
import { Values } from "../form-container/form-container";

import { companySupportEmail } from "constants/company-info.constant";

import styles from "./retry-modal.module.css";
import closeImg from "public/assets/images/modal-close.svg";

interface Props {
  isOpen: boolean;
  setClose: (value: boolean) => void;
  submitForm: (values: Values) => void;
  values: Values;
  isSubmitting: boolean;
};

const RetryModal = ({ isOpen, setClose, values, submitForm, isSubmitting }: Props) => {
  const [disabled, setDisabled] = useState(false);
  const { debounce } = useDebounce(5000);

  const resendForm = () => {
    submitForm(values);
  }

  useDidUpdate(() => {
    if (!isSubmitting) {
      setDisabled(true);
      debounce(() => {
        setDisabled(false);
      });
    };
  }, [isSubmitting])

  return (
    <Modal
      isOpen={isOpen}
      setClose={setClose}
      className={styles.modal}
    >
      <div className={styles.content}>
        <div className={styles.head}>
          <button type="button" onClick={() => setClose(true)} className={styles.closeBtn}>
            <img src={closeImg} alt="" />
          </button>
          <div className={styles.title}>Please Confirm Your Payment</div>
        </div>
        <div className={styles.body}>
          <div className={styles.text}>
            Unfortunately your transaction was declined by your bank. Mostly
            It happens due to high activity on your bank account. Please contact
            your bank to approve transaction and  try to complete order again.
        </div>
          <Button
            className={styles.btn}
            appearance="success" onClick={resendForm}
            btnSize="big"
            disabled={disabled || isSubmitting}
          >
            RETRY PAYMENT
        </Button>
          <div className={styles.info}>
            If the problem still exist, please contact our customer service team: <br />
            <a className={styles.link} href={`mailto:${companySupportEmail}`}>{companySupportEmail}</a>
          </div>
        </div>
      </div>
    </Modal>
  )
};

export default RetryModal;