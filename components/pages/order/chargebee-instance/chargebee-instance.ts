import { appEnvironment } from "/config/environment.config";

const loadChargebee = (callback: () => void) => {
  const existingScript = document.getElementById("cb-snippet");
  if (!existingScript) {
    const script = document.createElement("script");
    script.src = "https://js.chargebee.com/v2/chargebee.js";
    script.id = "cb-snippet";
    document.body.appendChild(script);
    script.onload = () => {
      let chargebeeInstance = undefined;
      if (!chargebeeInstance) {
        // @ts-ignore
        chargebeeInstance = window.Chargebee?.init({
          site: appEnvironment.chargebeeSite,
          publishableKey: appEnvironment.chargebeeApiKey,
        });
        // @ts-ignore
        chargebeeInstance?.load("components");
      }
      callback();
    };
  } else {
    callback();
  }
};
export default loadChargebee;
