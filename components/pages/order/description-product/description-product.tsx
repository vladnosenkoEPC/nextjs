import React from "react";
import { BLOCKS, MARKS } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";

import styles from "./description-product.module.css";

type Props = {
  content: any;
};

const Image = ({ node }) => <img src={node.data.target.fields.file.url} alt="" />;
const Italic = ({ children }) => <span className={styles.italic}>{children}</span>;
const Bold = ({ children }) => <span className={styles.bold}>{children}</span>;

const options = {
  renderMark: {
    [MARKS.ITALIC]: (text) => <Italic>{text}</Italic>,
    [MARKS.BOLD]: (text) => <Bold>{text}</Bold>,
  },
  renderNode: {
    [BLOCKS.EMBEDDED_ASSET]: (node) => <Image node={node} />,
  },
};

const DescriptionProduct = ({ content }: Props) => {
  return (
    <div className={styles.wrapper}>
      {content.map((item, key) => contentParser(item.fields, options, key))}
    </div>
  );
};
const contentParser = (fields, options, key) => {
  const { className, content } = fields;
  if (!className) return null;
  let classesArr = className.split(" ");
  classesArr.forEach((part, index, arr) => {
    arr[index] = styles[classesArr[index]];
  });
  return (
    <div className={classesArr.join(" ")} key={key}>
      {documentToReactComponents(content, options)}
    </div>
  );
};

export default DescriptionProduct;
