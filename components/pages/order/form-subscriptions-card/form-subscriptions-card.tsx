import React from "react";

import { useField } from "formik";

import Checkbox from "./components/checkbox";

import styles from "./form-subscriptions-card.module.css";

type Props = {
  name: string;
  title: string;
  price: string;
  planId: string;
};

const FormSubscriptionsCard = ({
  name,
  title,
  price,
  planId,
}: Props) => {
  const [field, , helpers] = useField({ name });

  const handleClick = (): void => {
    helpers.setValue(planId);
  };

  const isChecked = field.value === planId;

  return (
    <button type="button" className={styles.card} {...field} onClick={handleClick}>
      <div className={styles.checkbox}>
        <Checkbox checked={isChecked} />
      </div>
      <div className={styles.titleAndPrice}>
        <div className={styles.info}>
          <div className={styles.title}>{title}</div>
        </div>
        <div className={styles.prices}>
          <div className={styles.price}>{price}</div>
        </div>
      </div>
    </button>
  );
};

export default FormSubscriptionsCard;
