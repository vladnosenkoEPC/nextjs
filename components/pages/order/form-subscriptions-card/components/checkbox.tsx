import React from "react";

import styles from "./checkbox.module.css";

// import activeCheckboxImg from "/assets/images/checkbox-active-new.svg";
// import emptyCheckboxImg from "/assets/images/checkbox-empty.svg";

type Props = {
  checked: boolean;
};

const Checkbox = ({ checked }: Props) => {
  const src = checked ? "/assets/images/checkbox-active-new.svg" : "/assets/images/checkbox-empty.svg";

  return <img className={styles.img} src={src} alt="" />;
};

export default Checkbox;
