import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDidMount } from "react-hooks-lib";

import { Header, Footer } from "components";

import { getPageContent } from "contentful-provider";

import FormContainer from "./form-container/form-container";
import DescriptionProduct from "./description-product/description-product";
import DescriptionSecurity from "./description-security/description-security";
import loadChargebee from "./chargebee-instance/chargebee-instance";

import styles from "./order.module.css";

const Order = () => {
  const historyRouter = useHistory();

  const [CbLoaded, setCbLoaded] = useState(false);
  const [content, setContent] = useState();
  const [pageTitle, setPageTitle] = useState("");

  useEffect(() => {
    // eslint-disable-next-line no-restricted-globals
    history.pushState(null, "", "");
    window.onpopstate = function () {
      historyRouter.push(window.location.pathname);
    };
  }, [historyRouter]);

  useDidMount(() => {
    loadChargebee(() => {
      setCbLoaded(true);
    });

    getPageContent()
      .then((response: any) => {
        setContent(response.items[0].fields.content);
        setPageTitle(response.items[0].fields.title);
      })
      .catch(console.error);
  })


  return (
    // <div className={styles.bg}>
    //   {content && (
    //     <div className={styles.container}>
    //       <DescriptionProduct content={content} />
    //       {CbLoaded && <FormContainer />}
    //       <DescriptionSecurity content={content} />
    //     </div>
    //   )}
    //   <Footer />
    // </div>
    <div>TEST</div>
  );
};

export default Order;
