import React, { useRef, useState } from "react";
import { Formik, Form, FormikHelpers, FormikState } from "formik";
import { object, string } from "yup";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { useSelector } from "react-redux";
import { RootState } from "ducks/index";

import { setPaymentToken, setRegularPlanId, resetBillStore, resetCustomerStore, setUserId } from "ducks";

import { notification, Button } from "components";
import FormPayment from "../form-payment/form-payment";
import FormSubscriptions from "../form-subscriptions/form-subscriptions";
import RetryModal from "../retry-modal/retry-modal";

import { postSubscription, PostSubscriptionData } from "api";
import { countryIsoValidation } from "utils/country.utils";

import { mainRoute } from "../../../constants/routes.constant";

import styles from "./form-container.module.css";

import btnImg from "public/assets/images/order-btn-icon.svg";

export type Values = {
  email: string;
  firstName: string;
  lastName: string;
  billingToken: string;
  planId: string;
  billingAddress: {
    holderName: string;
    phone: string;
    city: string;
    state: string;
    country: string;
    zip: string;
    line1: string;
    line2: string;
    line3: string;
  };
};

const validationSchema = object().shape({
  planId: string().required("Plan is required"),
  email: string()
    .email("Email is invalid")
    .required("Email is required")
    .matches(
      /^(?=(?!.*([!@#_+,\-$%/^&*])\1{1,}))(?=(?!.*^([!@#_+,\-$%/^&*])))(?=(?!.*[!@#_+,\-$%/^&*]{2}))/,
      "Email is invalid",
    ),
  firstName: string().required("First Name is required"),
  lastName: string().required("Last Name is required"),
  billingAddress: object().shape({
    holderName: string().required("Cardholder Name is required"),
    city: string()
      .min(1, "City is too short")
      .max(50, "City is too long")
      .required("City is required"),
    country: string()
      .min(1, "Country is too short")
      .max(30, "Country is too long")
      .test(countryIsoValidation)
      .required("Country is required"),
    state: string()
      .min(1, "State is too short")
      .max(50, "State is too long")
      .when("country", (country: string, schema: any) => {
        if (country === "US") {
          return schema.required("State is required");
        }
        return schema;
      }),
    zip: string()
      .min(1, "Postal Code is too short")
      .max(20, "Postal Code is too long")
      .required("Postal Code is required"),
    line1: string()
      .min(1, "Address is too short")
      .max(150, "Address is too long")
      .required("Address is required"),
    line2: string().max(150, "Address is too long"),
  }),
});

const FormContainer = () => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);

  const chargebee = useRef<any>(null);
  const formikRef = useRef<any>(null);

  const history = useHistory();

  const plansList = useSelector((state: RootState) => state.plan);

  const initialValues: Values = {
    email: "",
    firstName: "",
    lastName: "",
    billingToken: "",
    planId: plansList[0]?.planId || "",
    billingAddress: {
      holderName: "",
      phone: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      line1: "",
      line2: "",
      line3: "",
    },
  };

  const handleError = (error: string) => {
    notification.error(error);
  };

  const handleTokenize = async (values: Values): Promise<string> => {
    let token = "";
    if (chargebee.current) {

      const additionalData = {
        firstName: values.firstName,
        lastName: values.lastName,
        billingAddr1: values.billingAddress.line1,
        billingAddr2: values.billingAddress.line2,
        billingCity: values.billingAddress.city,
        billingState: values.billingAddress.state,
        billingStateCode: "",
        billingZip: values.billingAddress.zip,
        billingCountry: values.billingAddress.country,
      };

      const data: any = await chargebee.current
        .tokenize(additionalData)
        .catch((error: any) => {
          handleError(error.message)
        });

      token = data?.token || "";
    }
    return token;
  };

  const handleResetPreviousShoppingSession = () => {
    dispatch(resetBillStore());
    dispatch(resetCustomerStore());
  };

  const handleSubscribe = async (data: PostSubscriptionData): Promise<void> => {
    const [payload, error, code] = await postSubscription(data);

    if (payload) {
      dispatch(setPaymentToken(payload.customer.token));
      dispatch(setRegularPlanId(data.planId));
      dispatch(setUserId(payload.customer.id));
      notification.success("Successfully bought the subscription");
      history.push(`/thank-you`);
    } else if (error) {
      if (code.status === 402) {
        setIsOpen(true);
      }
      handleError(error.detail || "Cannot create subscription");
    }
  };

  const handleSubmit = async (
    values: Values,
    { setSubmitting }: FormikHelpers<Values>,
  ): Promise<void> => {
    handleResetPreviousShoppingSession();

    const token = await handleTokenize(values);
    if (token) {
      const data = {
        email: values.email,
        firstName: values.firstName,
        lastName: values.lastName,
        billingToken: token,
        planId: values.planId,
        billingAddress: {
          holderName: values.billingAddress.holderName,
          phone: values.billingAddress.phone,
          city: values.billingAddress.city,
          state: values.billingAddress.state,
          country: values.billingAddress.country,
          zip: values.billingAddress.zip,
          line1: values.billingAddress.line1,
          line2: values.billingAddress.line2,
          line3: "",
        },
      };

      await handleSubscribe(data);
    }

    setSubmitting(false);
  };

  const FormikComponent: any = Formik;
  const FormComponent: any = Form;

  const onReady = (chargebeeComponent: any): void => {
    chargebee.current = chargebeeComponent.current;
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <div className={styles.formWrapper}>
      <FormikComponent
        ref={formikRef}
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {({ isSubmitting, values, submitForm }: FormikHelpers<Values> & FormikState<Values>) => (
          <FormComponent>
            <h3 className={styles.smallHeader}>Your Subscription Level:</h3>
            <FormSubscriptions />
            <FormPayment onReady={onReady} />
            <Button
              className={styles.submit}
              appearance="success"
              type="submit"
              btnSize="big"
              loading={isSubmitting}
              loader={true}
            >
              Complete Order
              <img src={btnImg} alt="" />
            </Button>
            <RetryModal
              isOpen={isOpen}
              setClose={handleClose}
              submitForm={submitForm}
              values={values}
              isSubmitting={isSubmitting}
            />
          </FormComponent>
        )}
      </FormikComponent>
    </div>
  );
};

export default FormContainer;
