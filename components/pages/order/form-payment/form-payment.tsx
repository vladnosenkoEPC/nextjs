import React, { useEffect, useState, useRef } from "react";
import { useDidMount } from "react-hooks-lib";
import { useDispatch } from "react-redux";
import {
  CardComponent,
  CardNumber,
  CardExpiry,
  CardCVV,
} from "@chargebee/chargebee-js-react-wrapper";
import { MessageField, CardCvcPreview, Label } from "components";
import { useFormikContext } from "formik";
import { scrollToElement } from "utils/scroll.utils";

import { setUserEmail } from "ducks";
import { FormField, useDebounce } from "components";
import { fonts, stylesCard, inputClasses, errorStyles } from "./config/chargebee.config";
import { getAddress } from "api";
import { Values } from "../form-container/form-container";

import { geocodingParse } from "utils/geocoding.utils";

import styles from "./form-payment.module.css";

type Props = {
  onReady: (chargebeeComponent: any) => void;
};

type ChargebeeFieldState = {
  cardType: string;
  complete: boolean;
  empty: boolean;
  error: { errorCode: string; message: string };
  field: string;
  type: string;
};

let googleCalls = 0;
const maxGoogleCalls = 20;

const elementsSize = "big";

const FormPayment = ({ onReady }: Props) => {
  const {
    values,
    setFieldValue,
    submitCount,
    setFieldTouched,
    setFieldError,
    isValidating,
  } = useFormikContext<Values>();
  const { debounce } = useDebounce(400);
  const formNode = useRef<any>(null);
  const dispatch = useDispatch();

  const cbRef = useRef<any>(null);

  useDidMount(() => {
    setFieldValue("billingAddress.country", "US");
  });

  useEffect(() => {
    const holder = values.firstName + " " + values.lastName;
    if (holder !== " ") setFieldValue("billingAddress.holderName", holder);
  }, [values.firstName, values.lastName, setFieldValue]);

  useEffect(() => {
    debounce(() => {
      const errorsTitles = formNode.current.getElementsByClassName("message-error");
      const errorInput = errorsTitles[0];
      const cbInputDate = errorsTitles[1];
      const cbInputCvc = errorsTitles[2];

      if (isValidating) {
        if (errorInput.textContent.length) {
          scrollToElement(errorInput);
        } else if (cbInputDate.textContent.length) {
          scrollToElement(cbInputDate);
        } else if (cbInputCvc.textContent.length) {
          scrollToElement(cbInputCvc);
        }
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submitCount]);

  const [card, setCard] = useState<Partial<ChargebeeFieldState>>({});
  const [expiry, setExpiry] = useState<Partial<ChargebeeFieldState>>({});
  const [cvv, setCvv] = useState<Partial<ChargebeeFieldState>>({});

  const handleAutocompleteAddress = (zip: string): void => {
    if (!values.billingAddress.city && !values.billingAddress.state) {
      debounce(async () => {
        if (zip.length === 5 && googleCalls < maxGoogleCalls) {
          googleCalls++

          const [payload] = await getAddress(zip);

          if (payload) {
            const { country, state, city } = geocodingParse(payload);
            setFieldValue("billingAddress.country", country);
            setFieldValue("billingAddress.city", city);
            setFieldValue("billingAddress.state", state);
          }
          if (payload && payload.status === "ZERO_RESULTS") {
            setFieldValue("billingAddress.country", "");
            setFieldValue("billingAddress.city", "");
            setFieldValue("billingAddress.state", "");
            setFieldTouched("billingAddress.zip", true, false);
            setFieldError("billingAddress.zip", "Zip/Postal Code not found");
          }
        }
      });
    }
  };

  const [isStateDropdown, setIsStateDropdown] = useState(true);

  const handleStateVisibility = (country: string): void => {
    if (country === "US") {
      setIsStateDropdown(true);
    } else {
      setIsStateDropdown(false);
      setFieldValue("billingAddress.state", "");
    }
  };

  const getErrorStyle = (errorMessage?: string): string => {
    if (errorMessage) {
      return errorStyles;
    }
    return "";
  };

  const getCardRequiredError = (field: "cc" | "expiry" | "cvv"): string => {
    if (submitCount) {
      if (field === "cc" && (card.empty || !card.type)) {
        return "Credit card is required";
      }
      if (field === "expiry" && (expiry.empty || !expiry.type)) {
        return "Expiry is required";
      }
      if (field === "cvv" && (cvv.empty || !cvv.type)) {
        return "CVV is required";
      }
    }
    return "";
  };

  const emailInputHandler = (e: string) => {
    dispatch(setUserEmail(e));
  };

  const handleReady = () => {
    onReady(cbRef);
  }

  return (
    <div className={styles.form} ref={formNode}>
      <h3 className={styles.topTitle}>Customer Information</h3>
      <div className={styles.arrowImgWrapper}>
        <img src="/assets/images/order-down-arrow.svg" alt="" />
      </div>
      <FormField name="email" placeholder="" label="Email*" onFieldChange={emailInputHandler} />
      <FormField name="firstName" placeholder="" label="First Name*" />
      <FormField name="lastName" placeholder="" label="Last Name*" />
      <FormField name="billingAddress.holderName" placeholder="" label="Cardholder Name*" />

      <CardComponent fonts={fonts} styles={stylesCard} onReady={handleReady} ref={cbRef}>
        <Label text="Card Number" size={elementsSize} />
        <div className={styles.wrapper}>
          <CardNumber
            placeholder="0000 0000 0000 0000"
            className={`${inputClasses} ${getErrorStyle(
              card.error?.message || getCardRequiredError("cc"),
            )}`}
            onChange={setCard}
          />
        </div>
        <MessageField
          message={card.error?.message || getCardRequiredError("cc")}
          size={elementsSize}
        />

        <div className={styles.formDouble}>
          <div>
            <Label text="Expiration Date" size={elementsSize} />
            <div className={styles.wrapper}>
              <CardExpiry
                placeholder="MM / YY"
                className={`${inputClasses} ${getErrorStyle(
                  expiry.error?.message || getCardRequiredError("expiry"),
                )}`}
                onChange={setExpiry}
              />
            </div>
            <MessageField
              message={expiry.error?.message || getCardRequiredError("expiry")}
              size={elementsSize}
            />
          </div>
          <div className={styles.formCvc}>
            <div>
              <Label text="CVV" size={elementsSize} />
              <div className={styles.wrapper}>
                <CardCVV
                  placeholder="CVV"
                  className={`${inputClasses} ${getErrorStyle(
                    cvv.error?.message || getCardRequiredError("cvv"),
                  )}`}
                  onChange={setCvv}
                />
              </div>
              <MessageField
                message={cvv.error?.message || getCardRequiredError("cvv")}
                size={elementsSize}
              />
            </div>
            <CardCvcPreview />
          </div>
        </div>
      </CardComponent>

      <FormField name="billingAddress.line1" placeholder="" label="Address 1*" />
      <FormField name="billingAddress.line2" placeholder="" label="Address 2" />
      <div className={styles.formDouble}>
        <FormField
          name="billingAddress.zip"
          placeholder=""
          label="Postal Code*"
          onFieldChange={handleAutocompleteAddress}
        />
        <FormField name="billingAddress.city" placeholder="" label="City*" />
      </div>
      <div className={styles.formDouble}>
        <FormField
          type="country"
          name="billingAddress.country"
          placeholder=""
          label="Country*"
          inputSize="big"
          onFieldChange={handleStateVisibility}
        />
        {isStateDropdown ? (
          <FormField
            type="state"
            name="billingAddress.state"
            placeholder=""
            label="State*"
            inputSize="big"
          />
        ) : (
            <FormField name="billingAddress.state" placeholder="" label="State" inputSize="big" />
          )}
      </div>
      <FormField name="billingAddress.phone" placeholder="" label="Phone" />
      <div className={styles.requireText}> - Required Field*</div>
    </div>
  );
};

export default FormPayment;
