import stylesInput from "components/form/input/input.module.css";
import styles from "../form-payment.module.css";

export const fonts = ["https://fonts.googleapis.com/css?family=Open+Sans"];
export const stylesCard = {
  base: {
    color: "#10182f",
    fontWeight: "400",
    fontFamily: "'Lato', sans-serif",
    fontSize: "16px",
    fontSmoothing: "antialiased",
    iconColor: "#10182f",

    ":focus": {
      color: "#10182f",
    },

    "::placeholder": {
      color: "#757575",
    },

    ":focus::placeholder": {
      color: "#757575",
    },
  },
  invalid: {
    color: "#10182f",
  },
};

export const inputClasses = `${stylesInput.input} ${stylesInput.primary} ${stylesInput.big} ${stylesInput.required} ${styles.input}`;
export const errorStyles = `${stylesInput.error}`;
