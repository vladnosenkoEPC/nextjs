import React from "react";
import { useHistory } from "react-router-dom";

import { Container } from "components";

import { scrollToElement } from "utils/scroll.utils";

import { mainRoute } from "../../constants/routes.constant";

import styles from "./long-read.module.css";

import btnIcon from "public/assets/images/video-page-btn-icon.svg";
import contentImg39 from "public/assets/images/long-read-bob-sign.svg";

const LongRead = () => {
  const history = useHistory();
  const handleGetStarted = () => {
    history.push(`/${mainRoute}/order-page/strategic-profits-trader`);
  };

  const scrollDownHandler = (e: any) => {
    scrollToElement(e.target, "start");
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <h1 className={styles.headerTitle}>How A Brain Tumor Helped Me Crack Wall Street</h1>
        <p className={styles.headerText}>
          And how my misfortune can be <span className={styles.underline}>your edge in 2021</span>
        </p>
        <img className={styles.pageBg} src="/assets/images/long-read-bg.jpg" alt="" />
        <div className={styles.downBtn} onClick={scrollDownHandler}>
          <img className={styles.arrow} src="/assets/images/long-read-arrow.svg" alt="" />
        </div>
      </div>
      <Container>
        <div className={styles.mainContent}>
          <div className={styles.topImgWrapper}>
            <img src="/assets/images/long-read-bob-portrait.jpg" />
            <p>Bob Iaccino</p>
          </div>
          <p className={styles.text}>Hi, my name is Bob.</p>
          <p className={styles.text}>
            After 12 years developing a system which last year generated a 73.3% win rate and beat
          </p>
          <p className={styles.text}>the S&P by 254%, I’ve decided to share my story.</p>
          <p className={styles.text}>
            I urge you to read on because I believe 2021 will be a crucial year for all of us.
          </p>
          <p className={styles.text}>
            I woke up that morning with a terrible headache. The pain was unbearable and while I was
            on the verge of one of the biggest years in my trading career, there was just no way to
            focus through that kind of pain.
          </p>
          <p className={styles.text}>
            I remember how my wife, whispering in my ear, “do you want to go to the hospital” felt
            like a sledgehammer to the side of my head.
          </p>
          <p className={styles.text}>
            A few hours later, after vomiting from the pain, my wife rushed me to the hospital.
          </p>
          <p className={styles.text}>
            A long afternoon of painful tests later, and nothing. No one could tell me what was
            wrong.
          </p>
          <p className={styles.text}>
            They pumped my body with pain killers, scheduled an MRI and sent me home.
          </p>
          <p className={styles.text}>
            Fast forward to Easter weekend. My wife and I would drive to my mother’s house to spend
            our day with her and my father.
          </p>
          <p className={styles.text}>
            I woke up that morning and couldn’t see out of my left eye. It wasn’t dark, but more as
            if someone placed a white sheet of paper over the left side of my face.
          </p>
          <p className={styles.text}>I kept it to myself thinking it would clear up.</p>
          <p className={styles.text}>I was wrong.</p>
          <p className={styles.text}>
            I knew the situation was too serious for me to continue hiding it from my wife. Once I
            told her, she forced me to pull over and drove me to the nearest hospital.
          </p>
          <p className={styles.text}>
            Just like last time, test after test showed nothing. This time though, they immediately
            sent me to get an MRI.
          </p>
          <p className={styles.text}>
            The following morning my doctor said the scariest five words you’ll ever hear:
          </p>
          <h2 className={styles.title}>“You Have a Brain Tumor”</h2>
          <p className={styles.text}>
            It’s very difficult to describe what you feel and what you go through when you hear
            those words.For me,the typical thoughts of family, friends and how much time I have left
            flooded into my brain.
          </p>
          <p className={styles.text}>But something else hit me as well.</p>
          <p className={styles.text}>
            Work. My trading career had been moving along in big ways. From my early days on the CME
            (Chicago Mercantile Exchange) trading floor working for Japan’s third largest brokerage
            house, to a new role at Smith Barney, who had recently bought Nikko, my success to that
            point all hinged on one thing:
          </p>
          <p className={styles.text}>My brain.</p>
          <p className={styles.text}>
            As you can probably imagine, it felt uniquely cruel that the thing I relied on the most
            was now in a position of literally killing me.
          </p>
          <p className={styles.text}>
            I didn’t know it at the time, but it was actually a blessing in disguise.
          </p>
          <p className={styles.text}>
            I’m not going to bore you with all the details, now. But the important thing to know is
            the brain tumor devasted my pituitary gland.
          </p>
          <p className={styles.text}>
            The pituitary gland is no larger than a pea and is often referred to as the “master
            gland” of the human body.
          </p>
          <p className={styles.text}>
            One of its functions is to control what’s known as the “stress response”.
          </p>
          <p className={styles.text}>Harvard Medical School puts it this way:</p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-1.jpg" />
          </div>
          <p className={styles.text}>
            There’s a very complicated neurobiological response your body goes through when
            confronted with fear or stress.
          </p>
          <p className={styles.text}>In fact, this process literally alters how you behave.</p>
          <p className={styles.text}>
            It’s this process which makes some people freeze in the face of danger or jump into
            action.
          </p>
          <p className={styles.text}>So what does that mean?</p>
          <p className={styles.text}>
            Well, in a very real way, I have no physical sense of fear or stress.
          </p>
          <p className={styles.text}>
            In other words, the biological changes which alter how you think and process information
            under stress, no longer happen to me.
          </p>
          <p className={styles.text}>
            Let’s say you’ve been afraid of heights your entire life. Now imagine waking up one
            morning and being able to stand on top of the Empire state building without a care in
            the world.
          </p>
          <p className={styles.text}>
            How would your behavior change from that point forward? What would you do knowing you
            wouldn’t physically feel extreme stress or fear?
          </p>
          <p className={styles.text}>
            That’s precisely what happened to me. Intellectually I KNOW if I’m in danger or not, but
            unlike most people my body doesn’t trigger the stress response when faced with stressful
            situations or emotions.
          </p>
          <p className={styles.text}>
            What does this have to do with trading and the stock market?
          </p>
          <p className={styles.text}>
            If you’re in the stock market, you’ll fall into one of two groups.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-2.jpg" />
          </div>
          <p className={styles.text}>
            You either give into your emotions and follow the crowd. Or you ignore the crowd and
            gain from other’s fears.
          </p>
          <p className={styles.text}>
            Everyone likes to think they’re rational and dispassionate when it comes to trading.
            You’ve likely heard the advice of not getting “emotional” when it comes to trading.
          </p>
          <p className={styles.text}>However, the evidence is clear.</p>

          <h2 className={styles.title}>An MIT Study</h2>
          <p className={styles.text}>
            Whether by coincidence or not, my trading and investing success skyrocketed after my
            ordeal. And due to my own natural curiosity, I needed to find out why.
          </p>
          <p className={styles.text}>
            I came across an interesting study out of MIT which found a clear link between emotions
            and trading performance.
          </p>
          <p className={styles.text}>
            The more intense the emotions, the worse the trading performance.
          </p>
          <p className={styles.text}>And there’s more.</p>
          <p className={styles.text}>
            According to a popular trading platform, 80% of traders lose money in just one year. And
            the median loss it 36.30%!
          </p>
          <p className={styles.text}>Then there’s this troubling fact:</p>
          <p className={styles.text}>
            A North American Securities Administration Association survey concluded 70% of traders
            will lose nearly all of their money.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-3.jpg" />
          </div>
          <p className={styles.text}>
            You’ve likely heard legendary investor Warren Buffet once say, “Be fearful when others
            are greedy, and greedy when others are fearful.”
          </p>
          <p className={styles.text}>
            That’s great advice but most people can’t do that and in fact, it’s not even your fault.
          </p>
          <p className={styles.text}>
            Most people simply cannot control the stress response and the biological chain of events
            which occur.
          </p>
          <p className={styles.text}>
            The fact is this neurobiological response happens automatically and it alters the way
            you think. In most cases, this is a good thing, it’s your natural fight or flight
            response, but when it comes to trading it’s completely harmful.
          </p>
          <p className={styles.text}>
            Which is why over the last 28 years, I’ve been developing a system which removes the
            emotion on your part. Not only have I seen with my own eyes the danger of being too
            emotional with the markets, I also experienced how much more successful I’ve been since
            the tumor.
          </p>
          <p className={styles.text}>
            <span className={styles.bold}>
              I began fully running this system with real subscribers back in March of 2020 and
              here’s a quick snapshot of the results:
            </span>
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+18.97%</span> in March
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+4.51%</span> in April
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+3.98% </span> in May
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+3.81% </span>in June
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>-3.40%</span> in July
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+10.86%</span> in August
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>-2.72%</span> in September
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+4.30%</span> in October
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+7.15%</span> in November
          </p>
          <p className={`${styles.text} ${styles.list}`}>
            <span className={styles.bold}>+4.19%</span> in December
          </p>
          <p className={styles.text}>
            That’s a cumulative return of 62.79% which is 254% higher than the SPY, which is the
            most actively traded EFT that tracks the S&P index.
          </p>
          <p className={styles.text}>
            Keep in mind, I’m not talking about theoretical back-testing, like some people try to
            pass off as real results.
          </p>
          <p className={styles.text}>
            These are the real, published alerts and their results from the system.
          </p>
          <p className={styles.text}>
            As you can see, we had two losing months but the losses are small compared to the wins.
            That’s how the system is designed to work.
          </p>
          <p className={styles.text}>
            See, even though we have an over 70% win rate, the reality is, all trading systems have
            some level of risk and all systems generate losers.
          </p>
          <p className={styles.text}>
            However, taking this “emotionless” approach, we have been able to keep our losses to a
            minimum.
          </p>
          <p className={styles.text}>
            But even more important, I’ll explain in just a minute why using this system will be
            even more important this year in 2021.
          </p>
          <p className={styles.text}>
            This approach could transform your trading results and lead to life changing results.
            And by results, I mean a greater than a 70% win rate and beating the overall market by
            over 2.5X in 2020 alone.
          </p>
          <p className={styles.text}>
            As I mentioned earlier, I started my career in 1992 as a phone clerk on the CME trading
            floor.
          </p>
          <p className={styles.text}>
            I wasn’t very good at it. In fact, I struggled in the early years. I was a horrible
            trader when I first started.
          </p>
          <p className={styles.text}>
            But then I started to gain confidence as I began to understand the markets better. And
            then in 1996, my brain tumor changed everything.
          </p>
          <p className={styles.text}>
            It gave me an unfair advantage because I was finally able to remove emotions from
            trading. For me this was very much literal.
          </p>
          <p className={styles.text}>Over the next decade my career completely transformed.</p>
          <p className={styles.text}>
            I went from the Chicago trading pits to some of the biggest banks and brokerage firms in
            the world. I worked at Smith Barney and Germany’s Commerzbank.
          </p>
          <p className={styles.text}>
            I ran my own commodity pool at a Chicago based trading firm. I was a principal and a
            member of the investment committee for a very successful fund of hedge funds. And the
            list goes on.
          </p>
          <p className={styles.text}>
            I’ve also done over 2,000 interviews about the markets where you may have seen me on
            CNBC, Bloomberg, Fox Business, Yahoo! Finance, TD Ameritrade, and so on.
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-4.png" />
          </div>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-5.png" />
          </div>
          <p className={styles.text}>Through all this, I discovered a very simple truth.</p>
          <p className={styles.text}>
            Trading is hard. And anyone who tells you otherwise, is being less than truthful.
          </p>
          <p className={styles.text}>
            It can be an expensive rollercoaster ride of emotions. When you’re on a winning streak,
            you feel like you’re on the top of the world. But then you make a few bad decisions and
            it’s all gone.
          </p>
          <p className={styles.text}>
            Months of hard work and profits GONE… sometimes in just one day.
          </p>
          <p className={styles.text}>I’m sure you’ve been there.</p>
          <p className={styles.text}>It turns your stomach.</p>
          <p className={styles.text}>You feel dumb, like a failure even.</p>
          <p className={styles.text}>Maybe your position was too big.</p>
          <p className={styles.text}>Maybe you didn’t cut your losses.</p>
          <p className={styles.text}>Maybe you just held and hoped the market would turn around.</p>
          <p className={styles.text}>
            The bottom line is your emotions got in the way. It happens to everyone.
          </p>
          <p className={styles.text}>
            Trading can be nerve-wracking. And sometimes it can get even worse.
          </p>
          <p className={styles.text}>Take 2001 for example,</p>
          <p className={styles.text}>
            At the time, I was running a trading desk for Commerzbank Futures. Back then it was the
            third largest German bank in the world.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-6.jpg" />
          </div>
          <p className={styles.text}>The day started off as usual.</p>
          <p className={styles.text}>
            I was on the CME trading floor with other traders, Peter Ori on my left and Paula on my
            right.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-7.jpg" />
          </div>
          <p className={styles.text}>
            When suddenly, the phones lit up like a Christmas tree. Call after call came in,
            rapid-fire. Just one right after the other.
          </p>
          <p className={styles.text}>The pit erupted into an almost deafening roar.</p>
          <p className={styles.text}>
            This was something I was used to. Anytime a piece of news would hit the wires, or the
            Fed made an announcement, you’d get what I can only describe as pure pandemonium.
          </p>
          <p className={styles.text}>
            I did what I normally did. I “…checked my positions and our clients positions to make
            sure we weren’t overexposed to whatever was causing the roar, then waited for it to die
            down.”
          </p>
          <p className={styles.text}>Only this time, it didn’t stop. This wasn’t typical.</p>
          <p className={styles.text}>
            I looked up at the giant jumbotron on the trading floor and immediately knew this
            wouldn’t be my typical day.
          </p>
          <p className={styles.text}>
            A hijacked plane smashed into the World Trade Center. It was September 11th.
          </p>
          <p className={styles.text}>
            Then 15 minutes later, another plane crashed. about 45 minutes later an announcement
            blasts through the P.A. system on the trading floor: the United States is under attack.
          </p>
          <h2 className={styles.title}>And here’s where things get interesting</h2>
          <p className={styles.text}>
            We’re told to evacuate but the desk I was running had a large number of trades still
            open.
          </p>
          <p className={styles.text}>
            I asked anyone with kids to leave and go home to their families.
          </p>
          <p className={styles.text}>
            For everyone else, I let them know it was up to them to stay with me as we closed out
            these trades, but no matter what, I’d be the last one to go.
          </p>
          <p className={styles.text}>
            As hard to believe as this maybe, I wasn’t afraid, I wasn’t freaking out. I was calm.
          </p>
          <p className={styles.text}>
            And for the first time, but not the last time in my life I thought “Thank God I had that
            brain tumor”.
          </p>
          <p className={styles.text}>
            I knew the gravity of the situation, but like I said before, I didn’t exactly “feel” the
            fear or stress. Which is why I continued to manage the trades. And the last thing I did
            was close a trade that I opened that morning…which I wasn’t supposed to do. You see, the
            rules of that system said we were supposed to hold that trade for two business days… but
            this was an unprecedented event in history and I knew that the best thing to do when you
            have no data to go on, was to eliminate the risk.
          </p>
          <p className={styles.text}>But guess what?</p>
          <p className={styles.text}>
            Turns out it was a huge winner. And my clients made more than 10 times the money they
            were supposed to make on this trade.
          </p>
          <p className={styles.text}>
            I knew the trading floor was going to be shut down for four days. And we had no idea
            what was going to happen when it reopened. Doomsday thoughts ran through everyone’s
            mind. Will there be a full-fledged invasion? Are we safe? Will the market crash?
          </p>
          <p className={styles.text}>No one knew.</p>
          <p className={styles.text}>I learned a few things that day.</p>
          <p className={styles.text}>
            First, news stories and unexpected events shock and scare people. It doesn’t even have
            to be as big as a terror attack. The fact is, most people go into fight or flight mode
            quickly and easily.
          </p>
          <p className={styles.text}>
            Second, I learned anything can happen, at any time, even the impossible.
          </p>
          <p className={styles.text}>
            Before 9/11 the general public was completely oblivious to the possibility of a terror
            attack of this scope on our soil.
          </p>
          <p className={styles.text}>
            What’s worse is, no one can accurately predict an event like this.
          </p>
          <p className={styles.text}>So what do you do if you can’t predict these events?</p>
          <p className={styles.text}>How can you safely navigate the market without big losses?</p>
          <p className={styles.text}>
            Despite all the craziness in the world, in the news, and in politics, how can you
            consistently beat the market?
          </p>
          <p className={styles.text}>Without great stress…</p>
          <p className={styles.text}>Without typical hesitation…</p>
          <p className={styles.text}>
            Without constantly watching the market and living tick by tick…
          </p>
          <p className={styles.text}>Is it even possible?</p>
          <p className={styles.text}>
            Academic studies going back decades prove time and time again: active traders who watch
            the market tick by tick have a hard time making money. In fact, the opposite is
            generally true. They lose money and over time they tend to lose all their money.
          </p>
          <p className={styles.text}>The evidence also shows following the crowd is a bad idea.</p>
          <p className={styles.text}>A recent study makes this point obvious.</p>
          <p className={styles.text}>
            It examined Robinhood traders from May 2018 until August 2020.
          </p>
          <p className={styles.text}>
            It looked at events where traders crowded into specific stocks. The most recent example
            of this is the craziness around Game Stop (ticker: GME).
          </p>
          <p className={styles.text}>
            What they found is prices usually spike and then reverse. Usually, the reversals are
            extremely painful and in the case of the Robinhood study, the reversals actually became
            negative returns. We don’t chase things like GME because, number one, I don’t really
            feel the greed or FOMO that other people do thanks to my tumor.
          </p>
          <p className={styles.text}>
            I know the value of sticking to the process, but also, because we were around for
            Pets.com and the crash of 1999-2000. We remember what the aftermath of these frenzies
            look like.
          </p>
          <p className={styles.text}>
            Another study tracked 1,600 Brazilian day traders for a year.
          </p>
          <p className={styles.text}>And found 97% of them lost money! 97%!</p>
          <p className={styles.text}>
            In fact, only 1% earned more than the Brazilian minimum wage.
          </p>
          <p className={styles.text}>There’s more.</p>
          <p className={styles.text}>
            A study in Taiwan used complete transaction data for the entire Taiwan Stock Market over
            a 15 year period.
          </p>
          <p className={styles.text}>Their conclusion?</p>
          <p className={styles.text}>
            Less than 1% of the day trading population is able to reliably make money trading.
          </p>
          <p className={styles.text}>
            And don’t think for a second this only applies to “amateurs”. Academic studies
            overwhelmingly prove professional money managers underperform their benchmarks over a
            multiyear period. In fact, a staggering 85% of professionals fail to outperform.
          </p>
          <p className={styles.text}>I could keep going… but you get the idea.</p>
          <p className={styles.text}>Making money in the markets is HARD…</p>
          <p className={styles.text}>But it’s not impossible.</p>
          <p className={styles.text}>
            I’ve spent decades in this business and have only had 3 down years.
          </p>
          <p className={styles.text}>
            How did someone like me, a son of Italian immigrants from the West Side of Chicago,
            succeed for so long while the vast majority of traders failed? For me the answer is
            obvious. You need a system. ..a process
          </p>
          <p className={`${styles.text} ${styles.textLessDownSpacing}`}>
            <span className={styles.bold}>A trading system that can… </span>
          </p>
          <p className={`${styles.text} ${styles.list}`}>Consistently beat the market</p>
          <p className={`${styles.text} ${styles.list}`}>Take emotions out of trading</p>
          <p className={`${styles.text} ${styles.list}`}>
            And can survive the ups and downs of the news cycles and irrelevant events
          </p>
          <p className={styles.text}>
            I looked for this type of system everywhere but they aren’t just sitting on the shelves
            at Target.
          </p>
          <p className={styles.text}>
            What I was looking for wasn’t available to regular people like you and me.
          </p>
          <p className={styles.text}>
            Sure Wall Street and big hedge funds have these tools, but not us. While I was at Smith
            Barney, Nikko Securities and Commerzbank Futures I had access to the best tools in the
            world. The best software money could buy. The types of systems you couldn’t possibly
            imagine. But they were no longer available to me, once I went off on my own.
          </p>
          <p className={styles.text}>Have you ever felt like you just can’t win?</p>
          <p className={styles.text}>Well, you’re 100% right.</p>
          <p className={styles.text}>
            You can’t win and someone else who’s much richer, more connected, more powerful is
            winning. Day after day while the rest of us struggle and lose money.
          </p>
          <p className={styles.text}>
            And the fact is the playing field simply isn’t level. Not by a longshot.
          </p>
          <p className={styles.text}>But that didn’t stop me, it actually motivated me.</p>
          <p className={styles.text}>
            I became obsessed so I started building my system and I did pretty well but I couldn’t
            do it all on my own.
          </p>
          <p className={styles.text}>
            I needed help and the help I needed was lurking inside the very hedge funds <br />I was
            looking to beat.
          </p>
          <h2 className={styles.title}>Meet Oz: The Man Who Breaks Hedge Funds</h2>
          <p className={styles.text}>
            After eight years of relentless work on this “emotionless” system, I met a very special
            human being. Let’s call him Oz. The man behind the curtain.
          </p>
          <p className={styles.text}>
            He went to Northwestern University received a master’s in electrical engineering, he’s a
            math whiz, and an insomniac.
          </p>
          <p className={styles.text}>
            During those sleepless nights, he created and patented a technology that helps to powers
            red light traffic cameras. I hope that part doesn’t make you hate him.
          </p>
          <p className={styles.text}>
            Later I would learn he was the man hedge funds hired to break their systems. Literally!
          </p>
          <p className={styles.text}>
            We met after Oz bought one of my courses and showed up at one of my live events at the
            University of Illinois.
          </p>
          <p className={styles.text}>
            With around 100 people in the room from all over the country, he comes up to me just
            before lunch and says, &quot;Hey, my name is &quot;Oz&quot;. I bought your course and
            it’s great but I can make it better. Would you like to know how?&quot;
          </p>
          <p className={styles.text}>Many people in my position would have likely ignored him.</p>
          <p className={styles.text}>Not me though.</p>
          <p className={styles.text}>
            After the last session, I met up with him again to see how he could “improve” what I had
            developed and sure enough, he did exactly that!
          </p>
          <p className={styles.text}>
            The changes he made are very technical so I won’t bore you, but I knew I had to partner
            up with him. I knew we’d develop the type of system I was after.
          </p>
          <p className={styles.text}>
            What I didn&apos;t know it back then, was that this was the start of what I now call:
          </p>
          <h2 className={styles.title}>The 10-Stock System</h2>
          <p className={styles.text}>My brain tumor, my 9/11 experience, meeting Oz…</p>
          <p className={styles.text}>
            These events were random and unrelated but all of them were needed to create the type of
            system I knew people like you and I needed.
          </p>
          <p className={`${styles.text} ${styles.textLessDownSpacing}`}>
            <span className={styles.bold}>
              And remember, the criteria is simple. The system must:
            </span>
          </p>
          <p className={`${styles.text} ${styles.list}`}>Consistently beat the market</p>
          <p className={`${styles.text} ${styles.list}`}>Take emotions out of trading</p>
          <p className={`${styles.text} ${styles.list}`}>
            And can survive the ups and downs of the news cycles and irrelevant events
          </p>
          <p className={styles.text}>
            Let’s take the first point: the system must consistently beat the market.
          </p>
          <p className={styles.text}>Take a look at the 2020 track record…</p>
          <p className={styles.text}>
            I started sending alerts in March 2020 and we did as follows:
          </p>
          <p className={styles.text}>
            Remember earlier when I told you we had two months last year showing losses?
          </p>
          <p className={styles.text}>
            Well, the system is designed to be “emotionless” and so those losses didn’t hurt us. Not
            one bit. In fact, as you can see from the chart above, we beat the S&P by 254%.
          </p>
          <div className={styles.imgWrapper}>
            <img src="/assets/images/long-read-8.jpg" />
          </div>
          <p className={styles.text}>
            But what does that mean in real life terms? With real money?
          </p>
          <p className={styles.text}>
            Let’s say you have a $25k portfolio how does the 10-Stock System compare with some of
            the other things you could have done in 2020?
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-9.jpg" />
          </div>
          <p className={styles.text}>
            The first option is trading it on your own with no help. Could you have gotten lucky?
            Definitely, but study after study shows the more likely scenario is you would’ve taken
            on massive losses.
          </p>
          <p className={styles.text}>In most cases, you would’ve lost it all trying to trade.</p>
          <p className={styles.text}>
            And remember, that’s not me saying it. It’s MIT, popular platforms like Robinhood, and
            even the North American Securities Administration Association saying it. In fact,
            according to the NASAA there’s a 70% chance you would have lost nearly all of your
            money.
          </p>
          <p className={styles.text}>
            So please, even if you decide not to use my proprietary 10-Stock System, do NOT go at it
            alone. The risk is far too great.
          </p>
          <p className={styles.text}>
            Now, let’s say you decided to put it in an S&P index fund (SPY) … you would have made
            $4,425 or 17.7%.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-10.jpg" />
          </div>
          <p className={styles.text}>And that’s not bad at all.</p>
          <p className={styles.text}>
            But had you used my 10-Stock System, you could have made $15,698 or 62.8%.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-11.jpg" />
          </div>
          <p className={styles.text}>That’s 254% more than the S&P… a 2.5x return.</p>
          <p className={styles.text}>
            Remember, it’s the same $25,000 portfolio but the difference in gains is significant.
            And get this, you actually take on LESS risk using the 10-Stock System than putting it
            into an index fund.
          </p>
          <p className={styles.text}>
            Take January 2021 for example, the 10-Stock System returned 5.96% while the S&P after
            making new all-time highs, turned around and fell to finish the month -1.05%.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-12.jpg" />
          </div>
          <p className={styles.text}>
            That’s because we took profits all month and had very little exposure to the downturn
            when it came. And they always come.
          </p>
          <p className={styles.text}>
            And then we have the last option which is do nothing. To me, this is probably the worst
            category.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-13.jpg" />
          </div>
          <p className={styles.text}>It gives you the illusion you’re playing it safe.</p>
          <p className={styles.text}>But you’re not.</p>
          <p className={styles.text}>
            The problem with the “do nothing” crowd is they think they’re protected.
          </p>
          <p className={styles.text}>But they’re not.</p>
          <p className={styles.text}>Why?</p>
          <p className={styles.text}>
            Because their purchasing power is declining… in fact, it’s getting obliterated.
          </p>
          <p className={styles.text}>Take a look at the US money supply chart…</p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-14.jpg" />
          </div>
          <p className={styles.text}>
            The amount of dollars in checking accounts in the banking system exploded in 2020.
          </p>
          <p className={styles.text}>It increased at the fastest rate in the history…</p>
          <p className={styles.text}>
            Now zoom in on November 2020. Do you see the jump of more than one trillion?
          </p>
          <p className={styles.text}>
            That means inflation is around the corner. Frankly it’s already here you just don’t
            <br /> know it yet.
          </p>
          <p className={styles.text}>See, the government tells you inflation is only about 1-2%.</p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-15.jpg" />
          </div>
          <p className={styles.text}>
            But that’s because the governments metrics for calculating real world inflation are at
            best, outdated…at worst, a fraud.
          </p>
          <p className={styles.text}>
            I can’t believe I’m about to say this, because I love this country. My father came here
            as a legal immigrant with a green card and became a citizen, worked hard and joined the
            middle class. I still believe it’s the greatest country on earth and I consider myself a
            patriot.
          </p>
          <p className={styles.text}>
            But the way our government portrays the level of inflation to its people reminds me of
            how deceitful China is with its people.
          </p>
          <p className={styles.text}>
            So if you’re in the “do nothing” crowd, you’re in a really bad spot.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-16.jpg" />
          </div>
          <p className={styles.text}>You think you’re saving your money but you’re not.</p>
          <p className={styles.text}>
            You’re letting it sit in your bank account and it’s wasting away at an alarming rate.
          </p>
          <p className={styles.text}>
            You can no longer stand on the sidelines. You have to act now because this situation is
            only going to get worse.
          </p>
          <h2 className={styles.title}>
            Let me tell you what the <br />
            10-Stock System is
          </h2>
          <p className={styles.text}>The core portfolio never has more than 10 stocks.</p>
          <p className={styles.text}>
            And with every alert you get the entry points, price targets and stop losses, and any
            adjustments we’re recommending on any open trades.
          </p>
          <p className={styles.text}>In other words, each trade has a defined risk.</p>
          <p className={styles.text}>Why?</p>
          <p className={styles.text}>
            Because this hits on the second point of the perfect system: the system must take
            emotions out of trading.
          </p>
          <p className={styles.text}>
            Let me show you some recent examples:
            <br />
            Take a look at Intel…
            <br />
            In this example, we took our profits before the drop.
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-17.jpg" />
          </div>
          <p className={styles.text}>
            We made 11% while others might have lost over 20% if they held through the drop. And we
            know by studying trader psychology, that’s exactly what most people do.
          </p>
          <p className={styles.text}>
            This is one of the major benefits of the 10-Stock System. <br /> By following it, you
            take profits constantly and consistently. <br /> And while other people could be down
            50% in a crash, we could be up 30% because we limit the risk ahead of time and get out
            while everyone else keeps piling on loses.
          </p>
          <p className={styles.text}>
            Another strategy we use to manage our losses is timing based on price action, and stop
            loss adjustment. We get in and out of our trades within weeks and we raise our stop
            losses when needed to protect capital. In a moment, you’ll see why this is so powerful.
          </p>
          <p className={styles.text}>
            That way when we hit our profit targets, we get out and move on to the next trade.
          </p>
          <p className={styles.text}>
            We don’t look back and wonder if we could have made more because it’s irrelevant and
            counterproductive. Remember, the key is to remove the emotion, not give in to it.
          </p>
          <p className={styles.text}>The reverse is also true.</p>
          <p className={styles.text}>
            If the stop loss gets hit, we don’t watch to see if the price recovers.
          </p>
          <p className={styles.text}>We simply move on.</p>
          <p className={styles.text}>
            Losses are a part of the game. You should expect them always because it simply doesn’t
            matter. As you saw from the chart earlier, our system gave us two losing months and yet…
          </p>
          <h2 className={styles.title}>Beat The S&P By 254%!</h2>
          <p className={styles.text}>If you want to play the game, you will win and lose.</p>
          <p className={styles.text}>And my job is for you to win more often than you lose.</p>
          <p className={styles.text}>My system is designed to win roughly 70% of the time.</p>
          <p className={styles.text}>
            The key is precise methodology combined with appropriate risk management and based on
            our performance for 2020, I know this approach works extremely well.
          </p>
          <p className={styles.text}>
            Something else to consider is that we generally look at over 4,000 stocks. And then we
            narrow it down through our proprietary process to about 100 to 200. And ultimately, we
            pick the best ones. This allows us to focus on the most profitable trades the market has
            to offer without wasting time with the losers.
          </p>
          <p className={styles.text}>
            And since our hold times tend to be short, we’re able to profit over and over in a short
            period of time.
          </p>
          <p className={styles.text}>
            Take a look at another trade wed did, in INTC as an example:
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-18.jpg" />
          </div>
          <p className={styles.text}>On March 18, we got in at $45.50…</p>
          <p className={styles.text}>
            We were out at $53.14 on March 24, less than a week later. INTC, kept going higher, but
            we DO NOT CARE, because we took our cash, and allocated it to something else that was
            moving.
          </p>
          <h2 className={styles.title}>The One TRUTH No One Ever Told You… Until Now:</h2>
          <p className={styles.text}>
            Earlier I talked about the timing strategy we use with the 10-Stock System. Let me show
            you how powerful this is.
          </p>
          <p className={styles.text}>
            Most people use a “buy and hold” strategy as a way of removing emotion from the
            equation. In theory this makes sense, but in practice it’s can actually detrimental.
          </p>
          <p className={styles.text}>Let’s use these Intel trades as an example.</p>
          <p className={styles.text}>
            As you saw, we traded Intel multiple times last year. But let’s say that instead of
            trading using the 10-Stock System’s timing and stop loss strategies, you bought on
            January 1st, and sold on December 31st.
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-19.jpg" />
          </div>
          <p className={styles.text}>You’d be sitting on a 17.3% LOSS.</p>
          <p className={styles.text}>Compare that what the 10-Stock System did with Intel stock.</p>
          <p className={styles.text}>
            Remember, this is the same company – Intel – as well as the same period of time.
            However, instead of buying and holding, we use the 10-Stock System and what you achieve
            is this:
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-20.jpg" />
          </div>
          <p className={styles.text}>
            This time, by simply following the system, you could have made a gain of 11.3%.
          </p>
          <p className={styles.text}>But here’s the kicker:</p>
          <p className={styles.text}>
            Now imagine we did this with multiple names over a full 12 months. Annualized that 11.3%
            gain and it becomes a 68% gain.
          </p>
          <p className={styles.text}>
            In other words, we took what would have been a 17.3% LOSS and turned it into an
            annualized 68% GAIN.
          </p>
          <p className={styles.text}>
            Can you see the power of combining this system of “emotionless” trading with timing and
            strategic stop losses?
          </p>
          <p className={styles.text}>
            Let’s look at another example. This time it’s Prudential, take a look:
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-21.jpg" />
          </div>
          <p className={styles.text}>
            We were in at $42.50 on March 18 and out at $54 on March 25 again, in a week for a quick
            27.1% gain. Think about what that means.
          </p>
          <p className={styles.text}>
            The only time your money can grow is when it’s in the market, but the only time your
            hard-earned money is at risk is also when it’s in the market. If you can pull a 27.1%
            gain out of the market in a week, that’s an annualize return of 1,411%!
          </p>
          <p className={styles.text}>But it gets even better.</p>
          <p className={styles.text}>
            Had you gone with a “buy and hold” strategy like in the Intel example, you would have
            had a LOSS of 17% as shown here in this chart:
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-22.jpg" />
          </div>
          <p className={styles.text}>
            Compare that to the 10-Stock System where we traded in and out of Prudential twice last
            year with an average hold time of 75 days and you get a whopping 142% annualized return.
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-23.jpg" />
          </div>
          <p className={styles.text}>
            Simply taking profits and limiting losses and, we took what would have been a 17% loss
            and turned it into a 142% annualized return.
          </p>
          <p className={styles.text}>
            Of course, not every trade will be held for such a short period of time. Sometimes we’ll
            see a trade where it makes sense to stay in a bit longer. More like a month and even
            more.
          </p>
          <p className={styles.text}>Have a look at AMD. Advanced Micro Devices:</p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-24.jpg" />
          </div>
          <p className={styles.text}>
            We entered at $85 on November 20 and exited less than a month later on December 15
            $96.85. for a quick 13.9% gain.
          </p>
          <p className={styles.text}>
            If you can pull a 13.9% gain out of the market in less than a month, that’s an annualize
            return of 203%!
          </p>
          <p className={styles.text}>
            Are you starting to see why the 10-Stock System is able to beat the S&P and hand you
            hedge-fund like gains without having to constantly watch the markets?
          </p>
          <p className={styles.text}>
            In the case of AMD, had you gone with the buy and hold approach you would have done
            quite well. The total gain would have been an annualized return of 95%.
          </p>
          <p className={styles.text}>
            We can all agree that’s a great return but using the 10-Stock System you would have more
            than doubled that gain. And what’s even more impressive is all of this is done
            automatically for you without you having to think about it.
          </p>
          <p className={styles.text}>Now take a look at CSCO, Cisco:</p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-25.jpg" />
          </div>
          <p className={styles.text}>
            We got in on April 6 at $41.50 and got out a month later on May 5, at $40.95. As you can
            see this was a loss of only 1.3%.
          </p>
          <p className={styles.text}>
            The momentum faltered and we continued to adjust our stop loss until we got stopped out
            for a small loss.
          </p>
          <p className={styles.text}>
            And that’s the beauty of the 10-Stock System. We protect our downside. If we’re up on a
            trade, we adjust our stop loss. We don’t sit and watch our profits disappear.
          </p>
          <p className={styles.text}>
            But then take a look at the very next trade ARNA, Arena Pharmaceuticals:
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-26.jpg" />
          </div>
          <p className={styles.text}>
            We entered at $51.25 on May 5 and exited on May 21 at $58.00 which handed us a 13.2%
            gain.
          </p>
          <p className={styles.text}>
            On this trade, let’s say you would have gone the “buy and hold” approach… Again, if you
            bought on January 1st and sold on December 31st, you would have achieved an impressive
            gain of 67.3%
          </p>
          <p className={styles.text}>Not bad at all, however…</p>
          <p className={styles.text}>
            …Using the 10-Stock System’s timing and stop loss approach, we generated a 13.2% gain in
            less than three weeks, annualized that’s a 300% gain.
          </p>
          <p className={styles.text}>
            Another example we have from the system is EYE, National Vision Holdings:
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-27.jpg" />
          </div>
          <p className={styles.text}>
            We entered at $31.50 on July 24 and exited on August 7 at $36.50 for a 15.9% gain.
          </p>
          <p className={styles.text}>
            Had you bought EYE on January 1st and sold it December 31st, you would have had a nice
            gain of 38.2%.
          </p>
          <p className={styles.text}>Again, that’s not bad at all.</p>
          <p className={styles.text}>
            Following the 10-Stock System though, we achieved a 15.9% gain in 2 weeks, that’s an
            annualized 241% return.
          </p>
          <p className={styles.text}>
            These are all real stocks, traded with real money by our small group of members, and
            these are all real results. And since the goal is to be consistent over time and not
            take on more risk than necessary, the gains tend to be smaller.
          </p>
          <p className={styles.text}>This is something I really need to stress.</p>
          <p className={styles.text}>
            If these consistent winners week after week, month after month, and year after year bore
            you… then this is not for you.
          </p>
          <p className={styles.text}>
            I need to state clearly, I’m not going to hand you the next Game Stop. I have no
            interest that type of trading. It’s short-lived, irrational, non-sensical speculation
            and that simply isn’t what I do. Sure it’s fun when it’s going well, but eventually that
            crazy game of musical chairs comes to an end. The music stops as it always does and the
            reversals are some of the most painful things I’ve seen. I’ve seen this movie before and
            it’s a horror story for most.
          </p>
          <p className={styles.text}>
            Personally, I do think a small – very small – portion of your capital should be in
            speculative trades but that’s not what most people do.
          </p>
          <p className={styles.text}>What my 10-Stock System does is simple:</p>
          <p className={styles.text}>It wins.</p>
          <p className={styles.text}>Consistently and with well-managed downside risk.</p>
          <p className={styles.text}>
            The one drawback – if you can call it that – is the individual stock gains are smaller.
          </p>
          <p className={styles.text}>
            However, they add up to very big wins over time as shown by the fact the 10-Stock System
            outperformed the S&P by 254% last year.
          </p>
          <p className={styles.text}>
            You may not know this but the US stock market ended 2020 at all-time highs. Despite the
            pandemic, despite the shaky start to the year, it broke the prior all-time high record.
          </p>
          <p className={styles.text}>
            Remember that study I mentioned earlier? Over 85% of PROFESSIONAL money managers rarely
            outperform their benchmarks. What that means is the pros didn’t beat the market or at
            the very best, barely kept up with it.
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-28.jpg" />
          </div>
          <p className={styles.text}>
            The 10-Stock System outperformed the market. By a huge margin of 254%.
          </p>
          <p className={styles.text}>
            Like I said before, if consistent wins just isn’t for you, I hope you got some value out
            of what I shared here, but we’re not a good fit.
          </p>
          <p className={styles.text}>
            However, if you understand massive homeruns based on irrational speculation is extremely
            dangerous to your financial health & well-being, if you can see the value of
            consistently winning on your trades all year long, then read on.
          </p>
          <p className={styles.text}>
            Inside of the 10-Stock System you’ll get 2-8 trade alerts per month. The reason for the
            high level of activity is what I mentioned before.
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-29.jpg" />
          </div>
          <p className={styles.text}>
            The wins are smaller but we’re increasing the timing of the trades for bigger gains
            which is why you’ll get 2-8 alerts per month.
          </p>
          <h2 className={styles.title}>How My Brain Tumor Helps You</h2>
          <p className={styles.text}>
            If you recall, my brain tumor destroyed my pituitary gland and because of it, I have no
            physical reaction to stress or fear.
          </p>
          <p className={styles.text}>
            I knew from that experience, I needed to develop a system that mimics that same feeling
            for you.
          </p>
          <p className={styles.text}>
            Now, I can’t do anything about your body’s natural response to stress – and I certainly
            will NEVER wish brain tumor on anyone – but I came up with an ingenious way for you to
            experience little to no stress when following the trade alerts I send you.
          </p>
          <div className={styles.imgWrapper}>
            <img src="/assets/images/long-read-30.jpg" />
          </div>
          <p className={styles.text}>How’s that even possible?</p>
          <p className={styles.text}>All of the trade alerts are sent at night.</p>
          <p className={styles.text}>
            You don’t have to watch the markets tick by tick wondering if you made the right
            decision, and you won’t be distracted by intraday craziness.
          </p>
          <p className={styles.text}>
            Right before bedtime – which, incidentally, is the time when the LEAST amount of stress
            hormones are in your body – you review the alert I’ll send you, punch in a few numbers,
            enter your trade, and then…
          </p>
          <p className={styles.text}>
            Well, then you sleep with peace of mind knowing that your trade, your ENTIRE trade –
            position size, risk exposure, target profit, etc – is already set.
          </p>
          <p className={styles.text}>
            The only thing you have to do before the markets open is make sure you’re not getting in
            below the stop loss price. That’s it.
          </p>
          <p className={styles.text}>
            If there’s anyone who knows the huge advantage you get by removing stress and emotions
            from trading, it’s me.
          </p>
          <p className={styles.text}>
            It’s this edge that’s given me a winning track record for the last 8 of the last 10
            years.
          </p>
          <p className={styles.text}>
            It’s why the financial news shows always call me for interviews and feedback on the
            markets. Even as recently as August 21, 2020, right in the middle of the pandemic, who
            did Yahoo News call?
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperSizeFix}`}>
            <img src="/assets/images/long-read-31.png" />
          </div>
          <p className={styles.text}>
            I truly believe sending you the alerts this way is how you’ll win over and over and over
            again. By setting up the trade – the ENTIRE trade – before you go to sleep and only
            checking it once in the morning allows you to go about your day without thinking or
            worry about what the stock market is doing.
          </p>
          <p className={styles.text}>
            Remember, the way I have you structure the trade, you’ll know exactly when to get in and
            out. Profits will be taken automatically. No need to stare at your screen all day long
            and run the chance of you letting your emotions get in the way.
          </p>
          <p className={styles.text}>
            The downside risk is covered, you’ll know ahead of time what your risk is.
          </p>
          <p className={styles.text}>
            The upside profit potential is also covered, you won’t have to wonder when to sell and
            take profits.
          </p>
          <p className={styles.text}>
            Again, all of this is done automatically the night before when you follow the trade
            alert I’ll send you.
          </p>
          <p className={styles.text}>
            So in the evening at around 7pm Eastern Time, you’ll get an email which looks like this:
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperBigger}`}>
            <img src="/assets/images/long-read-32.png" />
          </div>
          <p className={styles.text}>Quick and easy.</p>
          <p className={styles.text}>I’ll tell you exactly what you need to do.</p>
          <p className={styles.text}>And you’ll have plenty of time to act.</p>
          <p className={styles.text}>
            From the comfort of your home and in fact so simple you can even do it all on your
            phone. No need to even fire up your computer.
          </p>
          <p className={styles.text}>
            Just a few clicks to enter the number of shares and your entry price, your profit target
            and then let the market work for you.
          </p>
          <p className={styles.text}>
            Remember our profit targets are automatic so when the profit target gets triggered,
            money is automatically deposits into your account. You don’t do a thing.
          </p>
          <p className={styles.text}>
            Imagine waking up with $5k in profits or even $10k and realizing you didn’t have to be
            glued to your computer screen. Without experiencing the rollercoaster madness of the
            markets. Without letting your stresses and emotions get in the way.
          </p>
          <p className={styles.text}>
            Another thing to keep in mind is that you’ll always hear from me.
          </p>
          <p className={styles.text}>
            Even if there are no changes to the portfolio on a given day. I’ll still email you. And
            let you know nothing changed.
          </p>
          <p className={styles.text}>
            This is white glove service and I will hold your hand every step of the way.
          </p>
          <p className={styles.text}>
            And if you have questions. If something doesn’t make sense to you. Ask me.
          </p>
          <p className={styles.text}>
            And I will host a Q&A session to answer your questions about the service.
          </p>
          <p className={styles.text}>
            Also, once per month I’ll give you my thoughts on the market and the economy.
          </p>
          <p className={styles.text}>It’s a quick read that will keep you informed.</p>
          <p className={styles.text}>I cover topics you might not read about in the WSJ.</p>
          <p className={styles.text}>Or hear about in mainstream media.</p>
          <p className={styles.text}>I won’t sugarcoat anything. I’ll tell you the truth.</p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperBigger}`}>
            <img src="/assets/images/long-read-33.png" />
          </div>
          <p className={styles.text}>
            On top of that, you’ll receive a portfolio update video from me every two weeks. It’s
            not the prettiest face on the planet, but every two weeks I’ll show up in your email
            inbox like this:
          </p>
          <div className={`${styles.imgWrapper} ${styles.imgWrapperBigger}`}>
            <img src="/assets/images/long-read-34.png" />
          </div>
          <p className={styles.text}>
            The most recent topics in my video updates have been Gamestop frenzy.
          </p>
          <p className={styles.text}>
            And finally, once per quarter, I’ll review our performance. I look at our winners and
            losers and summarize how we did compared to the S&P.
          </p>
          <p className={styles.text}>Now, as you might imagine this type of service isn’t cheap.</p>
          <p className={styles.text}>The price is fair, but it’s certainly not cheap.</p>
          <p className={styles.text}>
            The value of a lifetime subscription is $10,000. The cost for a 1-year membership is $3
            grand. Like I said it’s not cheap. But it’s fair. As I mentioned earlier, if you used a
            $25k portfolio and followed the alerts for 2020, 10-Stock System could’ve made you
            $15,698.
          </p>
          <p className={styles.text}>So really, you shouldn’t overthink this. Here’s why...</p>
          <p className={styles.text}>
            The moment you become a member of the 10-Stock System, you get access to a suite of
            life-changing moneymaking resources and benefits. And that’s no exaggeration, as we’ve
            seen here today and from current members like Alan:
          </p>
          <div className={styles.imgWrapper}>
            <img src="/assets/images/long-read-35.jpg" />
          </div>
          <p className={styles.text}>
            10-Stock System members receive access to my entire portfolio alerts, with detailed buy
            up to prices, entry and exit prices. In the past, subscribers who followed these alerts
            have seen gains as high as 27.1%, 23.3%, 19.4% that’s enough to make a nice chunk of
            cash.
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-36.jpg" />
          </div>
          <p className={styles.text}>
            Remember, one of the biggest benefits of my 10-Stock System alerts is you get to take
            gains over and over. It allows you to take thousands of dollars in percentage gains off
            the table, and not worry about whether the value of your portfolio fluctuates.
          </p>
          <p className={styles.text}>
            And don’t forget, the video updates I send out every two weeks. I never hide and I’m
            just not wired that way. I’ll give you the good, the bad and the ugly on the markets. If
            the system is signaling for us to back off, that’s exactly what we’ll do and I’ll tell
            you all <br />
            about it.
          </p>
          <p className={styles.text}>
            Now, as the newest member of the 10-Stock System, you will be lined up to profit
            immediately.
          </p>
          <p className={styles.text}>
            The first thing you’ll get your hands on my position size calculator training video
            where we dive deep into one of the most important areas of trading.
          </p>
          <div className={`${styles.imgWrapper}`}>
            <img src="/assets/images/long-read-37.jpg" />
          </div>
          <p className={styles.text}>
            The topic can be quite boring but if you’ve ever asked yourself how much – or how little
            – of a particular stock to buy, or if you’re wondering what to do with the 2-8 alerts
            you’ll get from the 10-Stock system, this is a video you’ll want to watch.
          </p>
          <p className={styles.text}>
            Truth is at $3,000 a year, the 10-Stock System is a steal. And while I don’t know how
            long I’ll keep membership open, I can tell you, I’m already planning the next price hike
            because, frankly, $3,000 is simply too good.
          </p>
          <p className={styles.text}>
            But today, you won’t pay anywhere near that and that’s for two very important reasons.
          </p>
          <p className={styles.text}>
            First, because you watched this far I know you’re serious about finally being one of
            those people who knows EXACTLY what do amid all the Wall Street craziness that scares
            the hell out of everyone else, without the dangerous ups and downs most traders go
            through. For this reason I want to make it a complete no-brainer for you to join and
            take advantage of “emotionless” trading.
          </p>
          <p className={styles.text}>
            And second, because this is the first time I’ve officially opened up 10-Stock System to
            to a large group of new subscribers. Aside from the small circle of subscribers who have
            been profiting handsomely, this is the first time I’ve opened up access. For those two
            reasons, I’m going to offer you an incredible deal.
          </p>
          <p className={styles.text}>
            I’m going to slash half off the yearly new member fee. Instead of $3,000 for a very
            short time you’ll get a full year for HALF OFF... that’s just $1,500.
          </p>
          <p className={styles.text}>
            So today you get access to hedge-fund quality research which allows you to profit
            WITHOUT letting stress and emotions derail you. In other words, today only and for a
            very short time, you’ll get recommendations, guidance, and research into this market for
            around $28 per week.
          </p>
          <p className={styles.text}>
            Do you see why I told you earlier not to overthink this? Not to procrastinate? All you
            have to do is skip one night out at a restaurant per month and that’s enough to get you
            in front these nightly alerts that can lead to very nice profits. Of course, not every
            trade is a winner but with a 70% win rate, you’ll have what I call “profit windows” more
            often than not. And those “profit windows” add up to very big gains, life-changing
            gains, where you potentially never have to worry about money again. You can add those
            restaurant visits back in, and more.
          </p>
          <p className={styles.text}>
            So when you become the newest member of 10-Stock System right now you get to join for
            just $1,500. A 50% discount. You’ll get it for half off the regular price. Which comes
            to a measly $28 a week.
          </p>
          <p className={styles.text}>
            You should see an order button below this screen now. You’ll be taken to a secure
            membership enrollment form, where you can review all the details of this special deal so
            you can review the details of your offer and make sure it’s right for you.
          </p>
          <p className={styles.text}>
            You get all the membership benefits discussed here... instant access to the Position
            Calculator training video... the core portfolio... nightly alerts with full
            instructions… video updates every two weeks... and detailed portfolio and performance
            tracking...You get all of this for just $1,500 which is just $28 a week.
          </p>
          <div className={styles.imgWrapper}>
            <img src="/assets/images/long-read-38.jpg" />
          </div>
          <div className={styles.textBlock}>
            <h2 className={styles.textBlockTitle}>Plus… another bonus:</h2>
            <p className={styles.text}>
              And if you Sign up today, and I will send you an additional set of trades. I call them
              “high beta” trades. These are often faster moving and can include currency and
              commodities ETFs that don&apos;t make it to our initial screen, but still not only
              have the opportunity to provide you with extra profit, but can move faster and often
              farther then our core trades.
            </p>
            <p className={styles.text}>
              They are riskier, but because we control risk and still use responsible position
              sizing, they can be a powerful addition to any portfolio. Last year our high beta
              trades had a slightly lower winning percentage, but higher reward to risk and shorter
              hold times, than our core portfolio did, resulting in higher annualized returns. Our
              high beta trade made a full <span className={styles.bold}>24% more profit</span> on
              average per trade, than the core trades did! These can really move.
            </p>
            <p className={styles.text}>
              Remember, if you want a full breakdown, you can click on the button now. This is a
              very time-sensitive offer being made to you today. And I can tell you may not see the
              10-stock system again. Ever.
              <br />
              Don’t put this off. Don’t procrastinate.
              <span className={styles.bold}> At just $28 a week</span> – for a short time only –
              this truly is a no-brainer.
            </p>
          </div>
          <img src={contentImg39} alt="" />
          <p className={styles.text}>
            Thank you, <br />
            Bob
          </p>
          <div className={styles.btnWrapper}>
            <div onClick={handleGetStarted}>
              Get Started
              <img src={btnIcon} alt="" />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default LongRead;
