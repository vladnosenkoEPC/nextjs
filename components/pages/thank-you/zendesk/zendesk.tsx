const loadZendesk = () => {
  const existingScript = document.getElementById('ze-snippet');
  if (!existingScript) {
    const script = document.createElement('script');
    script.src = 'https://static.zdassets.com/ekr/snippet.js?key=4110ee39-0e87-4524-a349-82bd3e971600';
    script.id = 'ze-snippet';
    document.body.appendChild(script);
    script.onload = () => { 
      // @ts-ignore
      zE(function () {
        // @ts-ignore
        zE.hide();
      });
    };
  }
};
export default loadZendesk;