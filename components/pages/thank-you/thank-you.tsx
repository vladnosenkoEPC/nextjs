import React, { useEffect, useState } from "react";
import { useDidMount } from "react-hooks-lib";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

import { RootState } from "ducks";

import { notification, useDebounce, Container, Button } from "components";
import Description from "./description/description";

import loadZendesk from "./zendesk/zendesk";
import loadPurchaseTracking from "./purchase-tracking/purchase-tracking";

import { activationMail } from "api";

import { RESEND_TIME } from "constants/time.constant";

import styles from "./thank-you.module.css";

import helpImg from "public/assets/images/thank-you-help-icon.svg";

const ThankYou = () => {
  const { debounce } = useDebounce(RESEND_TIME * 1000);
  const [resendTimer, setResendTimer] = useState(RESEND_TIME);
  const historyRouter = useHistory();

  const [resendBtnVisible, setResendBtnVisible] = useState(true);

  const userEmail = useSelector((state: RootState) => state.customer.userEmail);
  const userId = useSelector((state: RootState) => state.customer.userId);
  const plansList = useSelector((state: RootState) => state.plan);

  useEffect(() => {
    // eslint-disable-next-line no-restricted-globals
    history.pushState(null, "", "");
    window.onpopstate = function () {
      historyRouter.push(window.location.pathname);
    };
  }, [historyRouter]);

  useEffect(() => {
    debounce(() => {
      setResendBtnVisible(true);
      setResendTimer(RESEND_TIME);
    });
  }, [resendBtnVisible, debounce]);

  useDidMount(() => {
    loadZendesk();
    loadPurchaseTracking(userId, plansList);
    if (!resendBtnVisible) {
      const myInterval = setInterval(() => {
        if (resendTimer > 0) {
          setResendTimer(resendTimer - 1);
        }
      }, 1000);
      return () => {
        clearInterval(myInterval);
      };
    }
  });

  const handleOpenZe = () => {
    //@ts-ignore
    window.zE.activate();
  };

  const handleResendEmail = async () => {
    const [payload, error] = await activationMail({ email: userEmail });

    if (payload && payload.status) {
      setResendBtnVisible(false);
      return notification.success(
        "Your activation email has been successfully resent. Please check your Inbox.",
      );
    }
    if ((payload && !payload.status) || error) {
      setResendBtnVisible(false);
      return notification.error("Something went wrong. Please contact customer support team.");
    }
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.innerWrapper}>
        <Container>
          <Description userEmail={userEmail} />
          {resendBtnVisible && (
            <Button className={styles.resendEmailBtn} onClick={handleResendEmail}>
              Resend activation email
            </Button>
          )}
          {!resendBtnVisible && (
            <div>
              <p className={styles.resendTimeoutInfo}>
                Done! Next resend will be available in {resendTimer} sec
              </p>
              <p className={styles.resendAdditionalInfo}>
                *If you do not receive the confirmation message within a few
                <br /> minutes of signing up, please check your Spam folder.
              </p>
            </div>
          )}
        </Container>
      </div>
      <div className={styles.bottomNav}>
        <Button className={styles.bottomHelpBtn} onClick={handleOpenZe}>
          <img src={helpImg} alt="" />
          Need help?
        </Button>
      </div>
    </div>
  );
};

export default ThankYou;
