import React from "react";

import styles from "./description.module.css";

import checkmarkImg from "public/assets/images/invoice-checkmark.svg";

type Props = {
  userEmail: string;
};

const Description = ({ userEmail }: Props) => {
  return (
    <div className={styles.container}>
      <img src={checkmarkImg} alt="" />
      <h1 className={styles.title}>Thank You!</h1>
      <p className={styles.text}>Please check your inbox: </p>
      <p className={styles.email}>{userEmail}</p>
      <p className={`${styles.text} ${styles.textSmall}`}>
        To complete the registration process, please click
        <br /> the link in the email we just sent you.
      </p>
    </div>
  );
};

export default Description;
