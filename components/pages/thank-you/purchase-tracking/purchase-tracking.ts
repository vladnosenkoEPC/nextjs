declare global {
  interface Window {
    dataLayer: any;
  }
}

const loadPurchaseTracking = (userId: string, plansList: any) => {
  const { priceAmount, sku, name, category } = plansList && plansList[0];
  
  const generateUserId = () => {
    const userTimestamp = "-" + Math.floor(Date.now());
    return userId + userTimestamp;
  };

  const userUniqueId = generateUserId();

  if (typeof window !== "undefined" && typeof window.dataLayer !== "undefined") {
    window.dataLayer.push({
      event: "purchase",
      transactionId: userUniqueId,
      transactionAffiliation: "PP",
      transactionTotal: priceAmount,
      transactionTax: 0,
      transactionShipping: 0,
      transactionProducts: [
        {
          sku: sku,
          name: name,
          category: category,
          price: priceAmount,
          quantity: 1,
        },
      ],
    });
  }
};

export default loadPurchaseTracking;
