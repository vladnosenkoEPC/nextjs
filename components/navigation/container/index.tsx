import React from "react";

import styles from "./style.module.css";

type Props = {
  children?: any;
  className?: string;
};

const Container = ({ children, className = "" }: Props) => {
  return <div className={`${styles.container} ${className}`}>{children}</div>;
};

export default Container;
