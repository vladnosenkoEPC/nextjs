import React from "react";

import lockImg from "public/assets/images/lock.svg";

import styles from "./style.module.css";

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.info}>
        <img src={lockImg} alt="" />
        Secure Order Form
      </div>
    </header>
  );
};

export default Header;
