import React from "react";

import styles from "./style.module.css";

const Footer = () => {
  const currentYear = new Date().getFullYear();

  return (
    <footer className={styles.footer}>
      <div className={styles.info}>© {currentYear} Strategic Profits Trader. All Rights Reserved.</div>
    </footer>
  );
};

export default Footer;
