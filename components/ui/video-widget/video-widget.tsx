import React, { useEffect, useRef, useState } from "react";
import { useDidMount } from "react-hooks-lib";
import { Button, useWindowSize, VideoLoader } from "components";
import { useHistory } from "react-router-dom";

import Player from "@vimeo/player";

import styles from "./video-widget.module.css";

import btnIcon from "public/assets/images/video-page-btn-icon.svg";

let player: any;

interface VideoProps {
  btnShowTime?: number;
  link?: string;
  id?: string;
  children?: any;
  changePageBgStatus?: Function;
  longReadPath?: string;
  checkoutPath?: string;
  redirect?: boolean;
}

const VideoWidget = ({
  btnShowTime,
  link = "https://player.vimeo.com/video/76979871",
  id = "player-76979871",
  children,
  changePageBgStatus,
  longReadPath = "",
  checkoutPath = "",
  redirect = false,
}: VideoProps) => {
  const containerRef = useRef<any>(null);
  const videoRef = useRef<any>(null);
  const [videoTiming, setVideoTiming] = useState(0);
  const [isFullscreen, setIsFullscreen] = useState(false);
  const videoEmbedLink = link;
  const { width } = useWindowSize();
  const history = useHistory();

  const [isBtnVisible, setIsBtnVisible] = useState(false);
  const [isSecondBtnVisible, setIsSecondBtnVisible] = useState(false);
  const [isThumbnailVisible, setIsThumbnailVisible] = useState(false);

  const [isVideoFinished, setIsVideoFinished] = useState(false);
  const [redirectTime, setRedirectTime] = useState(10);

  const [isLoaderVisible, setIsLoaderVisible] = useState(true);

  useEffect(() => {
    if (width <= 992) {
      setIsBtnVisible(true);
      setIsSecondBtnVisible(true);
    } else {
      setIsBtnVisible(false);
      setIsSecondBtnVisible(false);
    }
  }, [width]);

  useDidMount(() => {
    initialize();
  });

  useEffect(() => {
    if (btnShowTime) {
      if (videoTiming >= btnShowTime && btnShowTime != null) {
        setIsBtnVisible(true);
        player.exitFullscreen();
      }
    }
  }, [videoTiming, btnShowTime]);

  useEffect(() => {
    if (isVideoFinished && redirect) {
      const interval = setInterval(() => {
        if (redirectTime > 0) {
          setRedirectTime(redirectTime - 1);
        }
      }, 1000);
      return () => {
        clearInterval(interval);
      };
    }
  }, [isVideoFinished, redirectTime, redirect]);

  useEffect(() => {
    if (redirectTime === 0) {
      history.push(checkoutPath);
    }
  }, [redirectTime, checkoutPath, history]);

  const initialize = () => {
    if (videoEmbedLink) {
      player = new Player(videoRef.current);

      player.on("fullscreenchange", async () => {
        const fullScreenStatus = await player.getFullscreen();
        setIsFullscreen(fullScreenStatus);
      });

      player.on("timeupdate", (event: any) => {
        setVideoTiming(Math.floor(event.seconds));
      });

      player.on("pause", () => {
        if (changePageBgStatus) {
          changePageBgStatus(false);
        }
      });

      player.on("play", () => {
        if (changePageBgStatus) {
          changePageBgStatus(true);
        }
      });

      player.on("loaded", () => {
        setIsLoaderVisible(false);
        setIsThumbnailVisible(true);
      });

      player.on("ended", () => {
        setIsVideoFinished(true);
      });

      player.on("bufferstart", () => {
        setIsLoaderVisible(true);
        setIsThumbnailVisible(false);
      });

      player.on("bufferend", () => {
        setIsLoaderVisible(false);
      });
    }
  };

  const handleGetStarted = () => {
    history.push(checkoutPath);
  };

  const handleLink = () => {
    history.push(longReadPath);
  };

  const handleThumbnailClick = () => {
    player.play();
    setIsThumbnailVisible(false);
  };

  const wrapperStyles = `${!isFullscreen && styles.videoWrapper}`;
  const thumbnailStyles = `${styles.videoThumbnail} ${children && styles.activeVideoThumbnail}`;
  const btnStyles = `${styles.btnWrapper} ${isBtnVisible && styles.activeBtnWrapper}`;
  const secondBtnStyles = `${styles.secondBtn} ${isSecondBtnVisible && styles.activeSecondBtn}`;

  return (
    <div>
      <div className={styles.videoContainer} ref={containerRef}>
        <div className={wrapperStyles}>
          <iframe
            title={id}
            src={videoEmbedLink}
            width="100%"
            height="100%"
            // @ts-ignore
            allow="autoplay"
            allowFullScreen
            ref={videoRef}
          />
        </div>
        {isThumbnailVisible && (
          <div className={thumbnailStyles} onClick={handleThumbnailClick}>
            {children}
          </div>
        )}
        {isLoaderVisible && <VideoLoader />}
      </div>
      <div className={btnStyles}>
        <Button btnSize="big" loader={false} onClick={handleGetStarted}>
          Get Started
          <img src={btnIcon} alt="" />
        </Button>
        <div className={secondBtnStyles} onClick={handleLink}>
          Read Transcript
        </div>
      </div>
    </div>
  );
};

export default VideoWidget;
