import React, { useRef, memo } from "react";
import { useDidMount } from "react-hooks-lib";

import FlipClock from "flipclock";
import { useDebounce } from "components";

import "flipclock/dist/flipclock.css";
import styles from "./timer.module.css";

type Props = {
  time: number;
};

const Timer = memo(({ time }: Props) => {
  const timerRef = useRef(null);
  const { debounce } = useDebounce(time * 60000);

  const now = new Date();
  const offset = new Date(now.getTime() + time * 60000);

  const expired = now > offset ? now : offset;

  useDidMount(() => {
    const timer = new FlipClock(timerRef.current, expired, {
      face: "DayCounter",
      countdown: true,
    });

    debounce(() => {
      timer.stop();
    });
  });

  return <div className={styles.timer} ref={timerRef} />;
});

export default Timer;
