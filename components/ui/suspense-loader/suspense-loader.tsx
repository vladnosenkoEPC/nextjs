import React from "react";

import styles from "./suspense-loader.module.css";

const SuspenseLoader = () => {
  return (
    <div className={styles.background}></div>
  )
};

export default SuspenseLoader;