import React from "react";

import styles from "./email-placeholder.module.css";

import standardEmailImg from "public/assets/images/mail-icon.svg";

interface Props {
  email: string;
}

const EmailPlaceholder = ({ email }: Props) => {

  return (
    <div className={styles.emailContainer}>
      <div className={styles.emailIcon}>
        <img src={standardEmailImg} alt="" />
      </div>
      <div className={styles.emailName}></div>
    </div>
  );
};

export default EmailPlaceholder;
