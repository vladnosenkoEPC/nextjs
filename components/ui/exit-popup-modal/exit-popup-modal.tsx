import React from "react";
import { useDidMount } from "react-hooks-lib";

import { useDebounce, Modal } from "components";

interface Props {
  isOpen: boolean;
  setClose: (value: boolean) => void;
  children: any;
}

const exitPopupInitializationDelay = 3000;

export const ExitPopupModal = ({ isOpen, setClose, children }: Props) => {
  const { debounce } = useDebounce(exitPopupInitializationDelay);

  useDidMount(() => {
    debounce(() => {
      window.addEventListener("mouseout", exitPopupShow);
    });
    return () => window.removeEventListener("mouseout", exitPopupShow);
  });

  const exitPopupShow = (e: any): any => {
    if (!e.toElement && !e.relatedTarget) {
      setClose(true);
      window.removeEventListener("mouseout", exitPopupShow);
    }
  };

  return (
    <Modal isOpen={isOpen} setClose={setClose}>
      {children}
    </Modal>
  );
};

export default ExitPopupModal;
