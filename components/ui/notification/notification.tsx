/* eslint-disable react/display-name */
import React from "react";
import { toastr as reduxToastr } from "react-redux-toastr";
import { useDidMount } from "react-hooks-lib";

import store from "ducks";

import styles from "./notification.module.css";
import "react-redux-toastr/lib/css/react-redux-toastr.min.css";

// import successImg from "/assets/images/success.svg";
// import errorImg from "/assets/images/error.svg";

type Props = {
  text: string;
  type: "success" | "warning" | "error" | "info" | "message" | "confirm";
  remove?: () => void;
  id?: string;
};

type Options = {
  timeOut?: number;
  onShowComplete?: () => void;
  onHideComplete?: () => void;
  onCloseButtonClick?: () => void;
  onToastrClick?: () => void;
  showCloseButton?: boolean;
  closeOnToastrClick?: boolean;
  okText?: string;
  cancelText?: string;
  onOk?: () => void;
  onCancel?: () => void;
};

let openedMessages: any[] = [];

const images = {
  success: "/assets/images/success.svg" as string,
  warning: "/assets/images/error.svg" as string,
  error: "/assets/images/error.svg" as string,
  info: "",
  message: "",
};

const defaultOptions = {
  timeOut: 5000,
  newestOnTop: true,
  position: "top-center",
  transitionIn: "fadeIn",
  transitionOut: "fadeOut",
  progressBar: false,
  closeOnToastrClick: true,
  showCloseButton: false,
  icon: false,
};

export const Toastr = ({ text, type, remove, id }: Props) => {
  const image = images[type as keyof typeof images];

  useDidMount(() => {
    if (remove && id) {
      openedMessages.push({ id, remove });
    }
  });

  return (
    <div className={`${styles.toastr} ${styles[type as keyof typeof styles]}`}>
      {image && (
        <div className={styles.icon}>
          <img src={image} alt="" />
        </div>
      )}
      <div className={styles.text}>{text}</div>
    </div>
  );
};

export const notification = {
  success(text: string, options: Options = {}) {
    const id = getIdAndHideLimit();
    reduxToastr.success("", "", {
      ...defaultOptions,
      ...options,
      id,
      // @ts-ignore
      component: props => <Toastr {...props} id={id} text={text} type="success" />,
    });
  },
  warning(text: string, options: Options = {}) {
    const id = getIdAndHideLimit();
    reduxToastr.warning("", "", {
      ...defaultOptions,
      ...options,
      id,
      // @ts-ignore
      component: props => <Toastr {...props} id={id} text={text} type="warning" />,
    });
  },
  error(text: string, options: Options = {}) {
    const id = getIdAndHideLimit();
    reduxToastr.error("", "", {
      ...defaultOptions,
      ...options,
      id,
      // @ts-ignore
      component: props => <Toastr {...props} id={id} text={text} type="error" />,
    });
  },
  info(text: string, options: Options = {}) {
    const id = getIdAndHideLimit();
    reduxToastr.info("", "", {
      ...defaultOptions,
      ...options,
      id,
      // @ts-ignore
      component: props => <Toastr {...props} id={id} text={text} type="info" />,
    });
  },
  message(text: string, options: Options = {}) {
    const id = getIdAndHideLimit();
    reduxToastr.message("", "", {
      ...defaultOptions,
      ...options,
      id,
      // @ts-ignore
      component: props => <Toastr {...props} id={id} text={text} type="message" />,
    });
  },
  confirm(text: string, options: Options = {}) {
    reduxToastr.confirm(text, {
      ...options,
    });
  },
};

const getIdAndHideLimit = () => {
  const length = openedMessages.length;

  const id = length ? openedMessages[length - 1].id + 1 : 1;
  const firstMessage = openedMessages[0];

  if (firstMessage && length > 2) {
    store.dispatch(firstMessage.remove());
    openedMessages = openedMessages.filter(message => message.id !== firstMessage.id);
  }

  return id;
};
