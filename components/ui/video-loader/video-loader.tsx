import React from "react";

import styles from "./video-loader.module.css";

export const VideoLoader = () => {
  return (
    <div className={styles.loaderWrapper}>
      <svg className={styles.loader} viewBox="0 0 100 100">
        <g>
          <ellipse cx="50" cy="50" rx="25" ry="25" />
        </g>
      </svg>
    </div>
  );
};

export default VideoLoader;
