import React from "react";

import ModalComponent from "react-modal";

import styles from "./modal.module.css";

interface Props {
  isOpen: boolean;
  setClose: (value: boolean) => void;
  children: any;
  className?: string;
}
ModalComponent.setAppElement("body");

export const Modal = ({ isOpen, setClose, children, className = "" }: Props) => {
  const handleClose = () => {
    setClose(false);
  };

  return (
    <ModalComponent
      isOpen={isOpen}
      onRequestClose={handleClose}
      className={`${styles.content} ${className}`}
      overlayClassName={styles.overlay}
      shouldCloseOnOverlayClick={false}
    >
      {children}
    </ModalComponent>
  );
};

export default Modal;
