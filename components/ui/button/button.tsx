import React from "react";

import styles from "./button.module.css";

// import loadingImg from "public/assets/images/loading-process.svg";

interface Props {
  children: any;
  appearance?:
  | "primary"
  | "secondary"
  | "white"
  | "success"
  | "warning"
  | "icon"
  | "link"
  | "delete";
  btnSize?: "big" | "medium" | "small" | "tiny";
  className?: string;
  type?: "button" | "submit" | "reset";
  loading?: boolean;
  loadingText?: string;
  loader?: boolean;
  disabled?: boolean;
  onClick?: () => void;
}

function showLoader(loading: boolean, appearance: Props["appearance"]): boolean {
  const allowedAppearances = ["primary", "secondary", "success"];
  if (loading && allowedAppearances.includes(appearance || "")) {
    return true;
  }
  return false;
}

export const Button = ({
  children,
  loading = false,
  appearance = "primary",
  btnSize = "small",
  className = "",
  type = "button",
  loadingText = "",
  loader = true,
  ...rest
}: Props) => {
  const text = loading ? loadingText : children;
  const loadingStyle = loading ? styles.loading : "";
  const appearanceStyle = styles[appearance];
  const sizeStyle = styles[btnSize];
  const loaderEnabled = showLoader(loading, appearance);
  const loaderStyle = loaderEnabled ? "" : "";
  const loaderStyleCenter = loader && !loadingText ? styles.centerLoader : "";

  return (
    <button
      {...rest}
      disabled={rest.disabled || loading}
      type={type}
      className={`${styles.button} ${appearanceStyle} ${sizeStyle} ${loadingStyle} ${loaderStyle} ${className} btn-${appearance}`}
    >
      {text}
      {loaderEnabled && loader && (
        <div className={`${styles.loader} ${loaderStyleCenter}`}>
          <img className={styles.loadingImg} src="public/assets/images/loading-process.svg" alt="" />
        </div>
      )}
    </button>
  );
};

export default Button;
