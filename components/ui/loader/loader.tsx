import React from "react";

import styles from "./loader.module.css";

// import loadingImg from "public/assets/images/loading-process.svg";

export const Loader = () => {
  return (
    <div className={styles.wrapper}>
      <img className={styles.loadingImg} src="public/assets/images/loading-process.svg" alt="" />
    </div>
  );
};

export default Loader;
