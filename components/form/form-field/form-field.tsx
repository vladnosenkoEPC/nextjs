import React from "react";
import { useField } from "formik";

import Input from "../input/Input";
import CountrySelect from "../country-select/country-select";
import { InputProps } from "../input/Input";
import { formatCreditCardNumber, formatExpirationDate, formatCVC } from "utils/credit-card.utils";

type Props = {
  name: string;
  type?:
  | "text"
  | "number"
  | "date"
  | "textarea"
  | "password"
  | "tel"
  | "card-number"
  | "expiry"
  | "cvc"
  | "country"
  | "state";
  placeholder?: string;
  rows?: number;
  onFieldChange?: (value: string, prevValue: string) => void;
};

type FormFieldProps = Props & InputProps;

export const FormField = ({
  type = "text",
  onFieldChange = () => undefined,
  ...rest
}: FormFieldProps) => {
  const [field, meta, helpers] = useField({ name: rest.name });

  const { touched, error } = meta;

  const errorMessage = (touched && error) || "";
  const status = errorMessage ? "error" : undefined;

  const onInputChange = (e: any): void => {
    const { setValue } = helpers;
    const value = e.currentTarget.value;

    onFieldChange(value, field.value);

    if (type === "card-number") {
      setValue(formatCreditCardNumber(value));
    } else if (type === "expiry") {
      setValue(formatExpirationDate(value));
    } else if (type === "cvc") {
      setValue(formatCVC(value));
    } else {
      setValue(value);
    }
  };

  const getInputType = (): string => {
    if (type === "card-number" || type === "expiry" || type === "cvc") {
      return "tel";
    }
    return type;
  };

  if (type === "country") {
    return (
      <CountrySelect
        {...(rest as InputProps)}
        type={getInputType()}
        field={{ ...field, onChange: onInputChange }}
        error={errorMessage}
        value={field.value}
        status={status}
      />
    );
  }

  if (type === "state") {
    return (
      <CountrySelect
        {...(rest as InputProps)}
        type={getInputType()}
        field={{ ...field, onChange: onInputChange }}
        error={errorMessage}
        value={field.value}
        status={status}
        isStates={true}
      />
    );
  }

  return (
    <Input
      {...(rest as InputProps)}
      type={getInputType()}
      field={{ ...field, onChange: onInputChange }}
      error={errorMessage}
      value={field.value}
      status={status}
    />
  );
};

export default FormField;
