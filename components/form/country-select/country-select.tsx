/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from "react";
import { FieldInputProps } from "formik";

import MessageField from "../message-field/message-field";
import Label from "../label/label";

import styles from "./input.module.css";
import { countries } from "constants/countries.constants";
import { states } from "constants/states.constants";

export interface InputProps {
  value?: string;
  appearance?: "primary" | "secondary" | "alternative";
  field?: FieldInputProps<any>;
  onChangeText?: (value: string) => void;
  type?: string;
  error?: string;
  noError?: boolean;
  inputSize?: "large" | "big" | "medium" | "small";
  status?: "error" | "warning" | "success" | "default";
  label?: string;
  className?: string;
  isStates?: boolean;
}

export const CountrySelect = ({
  value,
  appearance = "primary",
  field,
  onChangeText,
  error = "",
  className = "",
  noError,
  inputSize = "medium",
  status = "default",
  label = "",
  isStates = false,
  ...rest
}: InputProps) => {
  const controlledField = field === undefined;

  const [fieldValue, setValue] = useState<string>(value || "");

  const fieldLabel = isStates ? "state" : "country";

  const formattedData = () => {
    if (isStates) {
      return states.map(item => {
        return { name: item.name, value: item.name };
      });
    } else {
      return countries.map(item => {
        return { name: item.Name, value: item.Iso2 };
      });
    }
  };

  useEffect(() => {
    if (controlledField && !value) {
      setValue("");
    }
  }, [value, controlledField]);

  const handleChange = (e: any): void => {
    setValue(e.currentTarget.value);
    if (onChangeText) {
      onChangeText(e.currentTarget.value);
    }
    return;
  };

  const inputProps = {
    ...rest,
    value: fieldValue,
    onChange: handleChange,
    ...field,
  };

  const placeholderStyle = !field?.value && !fieldValue ? styles.placeholder : "";

  return (
    <div className={styles.column}>
      {label && <Label text={label} size={inputSize} />}
      <div className={styles.wrapper}>
        <div className={styles.dropdownSymbol}></div>
        <select
          {...inputProps}
          className={`${styles.input} ${styles[appearance]} ${styles[inputSize]} ${styles[status]} ${placeholderStyle} ${className} select-${appearance}`}
        >
          <option value="" label={`Select a ${fieldLabel}`} />

          {formattedData().map(item => (
            <option key={item.name} value={item.value}>
              {item.name}
            </option>
          ))}
        </select>
      </div>
      {!noError && <MessageField message={error} size={inputSize} appearance={status} />}
    </div>
  );
};

export default CountrySelect;
