/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from "react";
import { FieldInputProps } from "formik";

import MessageField from "../message-field/message-field";
import Label from "../label/label";

import styles from "./input.module.css";

export interface InputProps {
  value?: string;
  appearance?: "primary" | "secondary" | "alternative";
  field?: FieldInputProps<any>;
  onChangeText?: (value: string) => void;
  type?: string;
  error?: string;
  noError?: boolean;
  inputSize?: "large" | "big" | "medium" | "small";
  status?: "error" | "warning" | "success" | "default";
  label?: string;
  className?: string;
}

export const Input = ({
  value,
  appearance = "primary",
  field,
  onChangeText,
  type,
  error = "",
  className = "",
  noError,
  inputSize = "big",
  status = "default",
  label = "",
  ...rest
}: InputProps) => {
  const controlledField = field === undefined;

  const [fieldValue, setValue] = useState<string>(value || "");

  useEffect(() => {
    if (controlledField && !value) {
      setValue("");
    }
  }, [value, controlledField]);

  const handleChange = (e: any) => {
    setValue(e.currentTarget.value);
    if (onChangeText) {
      onChangeText(e.currentTarget.value);
    }
    return;
  };

  const Input = type === "textarea" ? "textarea" : "input";

  const inputProps = {
    ...rest,
    value: fieldValue,
    onChange: handleChange,
    ...field,
    autoComplete: "off",
  };

  return (
    <div className={styles.column}>
      {label && <Label text={label} size={inputSize} />}
      <div className={`${styles.wrapper}`}>
        <Input
          {...inputProps}
          className={`${styles.input} ${styles[appearance]} ${styles[inputSize]} ${styles[status]} ${className} input-${appearance}`}
        />
      </div>
      {!noError && <MessageField message={error} size={inputSize} appearance={status} />}
    </div>
  );
};

export default Input;
