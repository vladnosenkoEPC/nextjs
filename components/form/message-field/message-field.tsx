import React from "react";

import styles from "./message-field.module.css";

interface Props {
  className?: string;
  message: string;
  appearance?: "error" | "warning" | "success" | "default";
  align?: "left" | "center" | "right";
  size?: "large" | "big" | "medium" | "small";
}

export const MessageField = ({
  message = "",
  appearance = "error",
  align = "left",
  className = "",
  size = "big",
}: Props) => {
  return (
    <div
      className={`${styles.field} 
      ${styles[appearance]} 
      ${styles[align]} 
      ${styles[size]} 
      ${className} 
      message-${appearance}`}
    >
      {message}
    </div>
  );
};

export default MessageField;
