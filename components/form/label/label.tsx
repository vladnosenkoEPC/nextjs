import React from "react";

import styles from "./label.module.css";

type Props = {
  text: string;
  align?: "left" | "center" | "right";
  size?: "large" | "big" | "medium" | "small";
  className?: string;
};

export const Label = ({
  text,
  align = "left",
  className = "",
  size = "big",
}: Props) => {
  return <div className={`${styles.label} ${styles[align]} ${styles[size]} ${className}`}>{text}</div>;
};

export default Label;
