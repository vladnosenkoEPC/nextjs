import React from "react";

import styles from "./card-cvc-preview.module.css";

// import CardImg from "/assets/images/credit-card-cvv.svg";
// import CardImgPreview from "/assets/images/credit-card-cvv-preview.svg";

const CardCvcPreview = () => {
  return (
    <div className={styles.wrapper}>
      <img className={styles.image} src="/assets/images/credit-card-cvv.svg" alt="" />
      <div className={styles.preview}>
        <img className={styles.image} src="/assets/images/credit-card-cvv-preview.svg" alt="" />
      </div>
    </div>
  );
};

export default CardCvcPreview;
