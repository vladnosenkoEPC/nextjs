import { useLocation, useHistory } from "react-router-dom";
import queryString from "query-string";

type QueryParam = string | (string | number | boolean)[] | number | boolean;

export interface QueryParams {
  [key: string]: QueryParam;
}

const options: any = {
  arrayFormat: "bracket",
  skipEmptyString: true,
};

export type UseQueryReturn = {
  query: QueryParams;
  search: string;
  setQueryParam: (param: string, value: QueryParam) => void;
  setQueryParams: (value: QueryParams) => void;
  updateQueryParams: (values: QueryParams) => void;
  stringify: (query: QueryParams) => string;
};

function useQuery() {
  const history = useHistory();
  const location = typeof window !== "undefined" && window.location.search;

  const search = location.search;

  const query = queryString.parse(search, options) as any;

  function setQueryParams(value: QueryParams) {
    const stringifiedValue = queryString.stringify(value, options);

    history.replace({
      search: stringifiedValue,
    });
  }

  function setQueryParam(param: string, value: QueryParam) {
    const newQuery = { ...query };

    newQuery[param] = value;

    const stringifiedValue = queryString.stringify(newQuery, options);

    history.replace({
      search: stringifiedValue,
    });
  }

  function updateQueryParams(values: QueryParams) {
    const newQuery = { ...query, ...values };

    const stringifiedValue = queryString.stringify(newQuery, options);

    history.replace({
      search: stringifiedValue,
    });
  }

  function stringify(query: QueryParams): string {
    return "?" + queryString.stringify(query, options);
  }

  const queryReturn: UseQueryReturn = {
    query,
    search,
    setQueryParams,
    setQueryParam,
    updateQueryParams,
    stringify,
  };

  return queryReturn;
}

export default useQuery;
