import { useRef } from "react";
/* eslint-disable @typescript-eslint/no-misused-promises */

type Debounce = {
  time: number;
  timer: any;
};

const useDebounce = (time?: number) => {
  const debounce = useRef<Debounce>({
    time: time || 600,
    timer: null,
  });

  const setDebounce = (callback: any) => {
    clearTimeout(debounce.current.timer);
    debounce.current.timer = null;
    debounce.current.timer = setTimeout(async () => {
      await callback();
      debounce.current.timer = null;
    }, debounce.current.time);
  };

  const resetDebounce = () => {
    clearTimeout(debounce.current.timer);
    debounce.current.timer = null;
  };

  return { debounce: setDebounce, resetDebounce, active: Boolean(debounce.current.timer) };
};

export default useDebounce;
