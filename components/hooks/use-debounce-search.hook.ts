import { useState, useRef } from "react";
import { useDidUpdate, useDidMount, useWillUnmount } from "react-hooks-lib";
import { stringify } from "querystring";

import useQuery from "components/hooks/use-query.hook";

function useDebounceSearch(customQuery?: { [key: string]: any }, time?: number) {
  const { query } = useQuery();
  const stringifiedParams =
    "?" + stringify({ "order[createdAt]": "desc", ...query, ...customQuery });

  const [queryParams, setQueryParams] = useState(stringifiedParams);
  const componentIsMounted = useRef(true);

  const debounce = useRef<any>({
    timer: null,
    time: time || 500,
  });

  const options = {
    dependencies: [queryParams],
  };

  useDidMount(() => {
    componentIsMounted.current = true;
  });

  useWillUnmount(() => {
    componentIsMounted.current = false;
  });

  useDidUpdate(() => {
    clearTimeout(debounce.current.timer);
    debounce.current.timer = setTimeout(() => {
      if (componentIsMounted.current) {
        setQueryParams(stringifiedParams);
      }
    }, debounce.current.time);
  }, [stringifiedParams]);

  return { search: queryParams, options };
}

export default useDebounceSearch;
