/* eslint-disable @typescript-eslint/no-misused-promises */
import axios from "axios";

import {
  handleHeaders,
  handleCancelDuplicatedRequest,
  handlePrepareData,
  handleStatus,
  handleErrors,
} from "./api.utils";
import { appEnvironment } from "config/environment.config";
import { handleRefreshToken } from "./api.refresh-token";

export interface ParamsProps {
  headers?: HeadersInit;
  url: string;
  method: "post" | "patch" | "put" | "delete" | "get";
  data?: any;
  canceling?: boolean;
  baseUrl?: string;
}

export interface Status {
  isCanceled: boolean;
  status: number;
}

export interface ErrorResponse {
  message: string;
  detail?: string;
  title?: string;
}

axios.interceptors.response.use((response: any) => {
  return response;
}, handleRefreshToken);

/**
 * Api middleware.
 * Used to get developer-friendly syntax and provide default parameters
 * to requests
 * @returns [payload, error, status]
 */

async function fetchMiddleware<T>(
  params: ParamsProps,
): Promise<[T | null, ErrorResponse | null, Status]> {
  return new Promise(resolve => {
    const { apiUrl } = appEnvironment;

    const base = params.baseUrl || apiUrl;
    const isFormData = params.data instanceof FormData;
    const headers: HeadersInit = handleHeaders(params, isFormData);
    const cancelToken = handleCancelDuplicatedRequest(params);
    const url = base + params.url;
    const data = handlePrepareData(params.data, isFormData);

    return axios({
      ...params,
      url,
      data,
      headers,
      cancelToken,
    })
      .then(({ data, status }) => {
        return resolve([data, null, { status, isCanceled: false }]);
      })
      .catch(err => {
        const status = handleStatus(err);
        const error = handleErrors(err.response?.data || err);
        const isCanceled = err.message === "Request has been canceled";

        return resolve([null, error, { status, isCanceled }]);
      });
  });
}

export default fetchMiddleware;
