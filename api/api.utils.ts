import axios from "axios";

import { TOKEN_STORAGE_FIELD } from "constants/auth.constant";
import { ParamsProps } from "./api.middleware";

type AbortControllersType = {
  [key: string]: any;
};

type ErrorProps = {
  message: string;
  response: any;
  request: any;
};

const { CancelToken } = axios;
const abortControllers: AbortControllersType = {};

function isExcludedEndpoint(url: string) {
  const excludedEndpoints = ["login", "account-activation", "reset-password"];
  const isExcluded = excludedEndpoints.find(excludedRoute => url.includes(excludedRoute));

  return Boolean(isExcluded);
}

export function handlePrepareData(payload: any, isFormData: boolean) {
  let data: string | undefined = undefined;
  if (isFormData) {
    data = payload;
  } else if (payload) {
    data = JSON.stringify(payload) as any;
  }
  return data;
}

export function handleHeaders(params: ParamsProps, isFormData: boolean) {
  const headers: HeadersInit = {};
  const token = localStorage.getItem(TOKEN_STORAGE_FIELD);
  const isUnauthorizedEndpoint = isExcludedEndpoint(params.url);

  if (token && !isUnauthorizedEndpoint) {
    headers.Authorization = `Bearer ${token}`;
  }

  if (!isFormData) {
    headers["Content-Type"] = "application/json";
  }

  Object.assign(headers, params.headers);

  return headers;
}

export function handleCancelDuplicatedRequest(params: ParamsProps) {
  if (params.canceling !== undefined && params.canceling === false) {
    return undefined;
  }

  const abortName = params.url.split("?")[0];

  if (abortControllers[abortName]) {
    abortControllers[abortName].cancel("Request has been canceled");
    delete abortControllers[abortName];
  }

  const cancelToken = CancelToken.source();

  abortControllers[abortName] = cancelToken;

  return cancelToken.token;
}

export function handleErrors(error: ErrorProps) {
  if (error.message === "cancel") {
    return new Error("Request canceled");
  } else {
    return error;
  }
}

export function handleStatus({ response, request }: ErrorProps) {
  if (response) {
    return response?.status;
  } else if (request) {
    return request?.status;
  }
  return 0;
}
