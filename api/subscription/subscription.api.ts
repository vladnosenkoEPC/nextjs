import fetchMiddleware from "../api.middleware";
import {
  PostSubscriptionData,
  PostSubscriptionAddonData,
  PostSubscriptionUpgradeData,
  CustomerData,
} from "./subscription.interfaces";

export * from "./subscription.interfaces";

export const postSubscription = (data: PostSubscriptionData) =>
  fetchMiddleware<CustomerData>({
    method: "post",
    url: `/payments/checkout`,
    data,
  });

export const patchSubscription = (subscriptionToken: string, data: PostSubscriptionUpgradeData) =>
  fetchMiddleware<null>({
    method: "patch",
    url: `/payments`,
    data,
    headers: {
      Authorization: `Bearer ${subscriptionToken}`,
    },
  });

export const postSubscriptionAddons = (
  subscriptionToken: string,
  data: PostSubscriptionAddonData,
) =>
  fetchMiddleware<null>({
    method: "post",
    url: `/payments/addon`,
    data,
    headers: {
      Authorization: `Bearer ${subscriptionToken}`,
    },
  });
