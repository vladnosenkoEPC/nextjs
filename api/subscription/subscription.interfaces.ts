export type PostSubscriptionData = {
  email: string;
  firstName: string;
  lastName: string;
  billingToken: string;
  planId: string;
  billingAddress: {
    holderName: string;
    phone: string;
    city: string;
    state: string;
    country: string;
    zip: string;
    line1: string;
    line2: string;
    line3: string;
  };
};

export type PostSubscriptionUpgradeData = {
  planId: string;
};

export type PostSubscriptionAddonData = {
  addonId: string;
  planId: string;
};

export type CustomerData = {
  customer: {
    id: string;
    token: string;
  };
};

export type ChargebeeData = {
  plan_id: string;
  card: {
    first_name: string; // "John",
    last_name: string; // "Doe",
    holder_name: string; // "John Doe",
    billing_addr1: string; // "Street",
    billing_addr2: string; // "Street",
    billing_city: string; // "Walnut",
    billing_state: string; // "California",
    billing_zip: string; // "91789",
    billing_country: string; // "US",
    expiry_month: number; // 12,
    expiry_year: number; // 12,
    cvv: string; // 12,
    number: string; // 12,
  };
  customer: {
    first_name: string; //"John",
    last_name: string; //"Doe",
    email: string; //"john@user.com",
    phone: string; //"123 123 123",
  };
  shipping_address: {
    first_name: string; // "John",
    last_name: string; // "Doe",
    email: string; // "john@user.com",
    phone: string; // "123 123 123",
    line1: string; // "Street",
    line2?: string; // "Street",
    line3?: string; // "Street",
    city: string; // "Walnut",
    state: string; // "California",
    zip: string; // "91789",
    country: string; // "US",
  };
  billing_address: {
    first_name: string; // "John",
    last_name: string; // "Doe",
    holder_name: string; // "John Doe",
    line1: string; // "Street",
    line2?: string; // "Street",
    line3?: string; // "Street",
    city: string; // "Walnut",
    state: string; // "California",
    zip: string; // "91789",
    country: string; // "US",
  };
};

export type ChargebeeError = {
  api_error_code: string;
  detail: string;
  error_code: string;
  http_code: number;
  http_status_code: number;
  message: string;
  type: string;
};
