export interface ActivationMailData {
  email: string;
}

export interface ActivationMail {
  status: boolean;
}
