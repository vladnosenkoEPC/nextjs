import fetchMiddleware from "../api.middleware";

import { ActivationMailData, ActivationMail } from "./activation-mail.interface";

export const activationMail = (data: ActivationMailData) =>
  fetchMiddleware<ActivationMail>({
    method: "post",
    url: `/resend-activation-mail`,
    data,
  });
