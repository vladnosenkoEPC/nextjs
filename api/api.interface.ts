export interface Status {
  isCanceled: boolean;
  status: number;
}

export interface ErrorResponse {
  message: string;
  detail?: string;
  title?: string;
}

export type PayloadResponse<T, E> = [T | null, E | ErrorResponse | null, Status];
