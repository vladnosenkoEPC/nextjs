/* eslint-disable no-restricted-globals */
import axios from "axios";

import { REFRESH_TOKEN_STORAGE_FIELD, TOKEN_STORAGE_FIELD } from "constants/auth.constant";
import { appEnvironment } from "config/environment.config";

const apiRefreshEndpoint = "/refresh_token";

export function handleRefreshToken(error: any): any {
  const originalRequest = error.config;
  const status = error?.response?.status;
  const isAuthorizationError = status === 401;
  if (isAuthorizationError && originalRequest.url.includes(apiRefreshEndpoint)) {
    return Promise.reject(error);
  }
  if (isAuthorizationError && !originalRequest._retry) {
    originalRequest._retry = true;
    return axios
      .post(appEnvironment.apiUrl + apiRefreshEndpoint, {
        refresh_token: localStorage.getItem(REFRESH_TOKEN_STORAGE_FIELD),
      })
      .then((res) => {
        const token = res?.data?.token;
        if (token) {
          localStorage.setItem(TOKEN_STORAGE_FIELD, token);
          originalRequest.headers["Authorization"] = "Bearer " + token;
          return axios(originalRequest);
        }
      });
  }
  return Promise.reject(error);
}
