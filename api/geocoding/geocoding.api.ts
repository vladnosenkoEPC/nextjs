import fetchMiddleware from "../api.middleware";
import { geolocationApiKey } from "constants/google.constants";
import { GeocodeData } from "./geocoding.interfaces";

const language = "en";
const country = "us";

export const getAddress = (zip: string) =>
  fetchMiddleware<GeocodeData>({
    method: "get",
    baseUrl: "https://maps.googleapis.com/maps/api",
    url: `/geocode/json?language=${language}&country=${country}&address=${zip}&sensor=true&key=${geolocationApiKey}`,
  });
