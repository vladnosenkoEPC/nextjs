import fetchMiddleware from "../api.middleware";

import { marketingMailDataExtensionData } from "./marketing-mail-data-extension.interface";

export const marketingMailDataExtension = (data: marketingMailDataExtensionData) =>
  fetchMiddleware<marketingMailDataExtensionData>({
    method: "post",
    url: `/add-email-to-data-extension`,
    data,
  });
