export interface marketingMailDataExtensionData {
  email: string;
  dataExtensionKey: string;
  planId: string;
}
