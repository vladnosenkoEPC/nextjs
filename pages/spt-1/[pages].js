import React from "react";
import { Provider } from "react-redux";
import store from "/ducks";
import ReduxToastr from "react-redux-toastr";
import Head from "next/head";
import dynamic from "next/dynamic";
import { getPageContent, getCurrentPageContent } from "contentful-provider";

export const getStaticPaths = async () => {
  const res = await getPageContent();

  const paths = res.items.map((entry) => {
    return {
      params: { pages: entry.fields.path.toString() },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async (context) => {
  const currentPage = context.params.page;
  const res = await getCurrentPageContent(currentPage);

  return {
    props: { content: res },
  };
};

const OrderPage = dynamic(() => import("/components/pages/order"));

const Page = ({ content }) => {
  const fields = content.items[0].fields;

  return (
    <Provider store={store}>
      <div className="sales-app">
        <div className="sales-view">
          <div>
            <Head>
              <title>{content.items[0].fields.title}</title>
            </Head>
            <OrderPage content={fields.content} />
          </div>
        </div>
      </div>
      <ReduxToastr />
    </Provider>
  );
};

export default Page;
