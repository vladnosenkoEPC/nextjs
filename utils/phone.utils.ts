/* eslint-disable no-useless-escape */
const phoneNumberRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

export const phoneNumberValidation = {
  message: "Phone number is invalid",
  test: function(value: any): boolean {
    if (!value) return true;
    return value.match(phoneNumberRegex);
  },
};
