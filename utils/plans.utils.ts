export const createPlansList = (response) => {
  const plansList = response.items[0].fields.content
  .find(item => item.fields.contentName === "PlansList").fields.content
  .reduce((filtered, item) => {
    if (item.fields.active) {
      const planItem = {
        title: item.fields.productTitle,
        planId: item.fields.productId,
        price: item.fields.productPriceText,
        priceAmount: item.fields.productPrice,
        category: item.fields.category,
        name: item.fields.name,
        sku: item.fields.sku,
      };
      filtered.push(planItem);
    }
    return filtered;
  }, []);

  return plansList;
}