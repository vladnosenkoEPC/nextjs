interface GeocodingResponse {
  country: string;
  city: string;
  state: string;
}

const defineCityType = (area: string): string | undefined => {
  if (area === "landmark") {
    return "landmark";
  } else if (area === "natural_feature") {
    return "natural_feature";
  } else if (area === "establishment") {
    return "establishment";
  } else if (area === "neighborhood") {
    return "neighborhood";
  } else if (area === "locality") {
    return "locality";
  }
};

export const geocodingParse = (payload: any): GeocodingResponse => {
  const addresses = payload.results?.[0]?.address_components;
  const formattedResponse = {
    country: "",
    city: "",
    state: "",
  };

  addresses?.forEach((i: any) => {
    if (i.types[0] === "country") {
      formattedResponse.country = i.short_name;
    }
    if (i.types[0] === defineCityType(i.types[0])) {
      formattedResponse.city = i.short_name;
    }
    if (i.types[0] === "administrative_area_level_1") {
      formattedResponse.state = i.long_name;
    }
  });

  return Object(formattedResponse);
};
