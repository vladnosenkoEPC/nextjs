import { countries } from "constants/countries.constants";

export const getCountryISO = (value?: string): string => {
  if (!value) return "";
  const countryCode = countries.find(country => country.Iso2 === value);
  return countryCode?.Iso2 || "";
};

export const countryIsoValidation = {
  message: "Country code is invalid",
  test: function(value: any): boolean {
    if (!value) return true;
    const country = getCountryISO(value);
    return Boolean(country);
  },
};
