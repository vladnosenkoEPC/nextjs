interface Props {
  view?: "center" | "end" | "nearest" | "start" | undefined;
}
export const scrollToElement = (node: HTMLElement, view?: Props["view"]) => {
  const block = view || "center";
  node.scrollIntoView({ block: block, behavior: "smooth" });
};
